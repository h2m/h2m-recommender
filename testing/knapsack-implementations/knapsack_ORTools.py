import os, sys
from ortools.algorithms.python import knapsack_solver

# append path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)))))
from EOptimMethod import *
from CTestResults import *

def solve_knapsack(values, capacities, ids, max_capacity, optim_method = EOptimMethod.REGULAR, use_reduction = True) -> CTestResults:
    solver_type = knapsack_solver.SolverType.KNAPSACK_MULTIDIMENSION_BRANCH_AND_BOUND_SOLVER
    if optim_method == EOptimMethod.REGULAR:
        solver_type = knapsack_solver.SolverType.KNAPSACK_DYNAMIC_PROGRAMMING_SOLVER
    elif optim_method == EOptimMethod.CBC:
        solver_type = knapsack_solver.SolverType.KNAPSACK_MULTIDIMENSION_CBC_MIP_SOLVER
    
    solver = knapsack_solver.KnapsackSolver(solver_type, "Knapsack")
    solver.set_use_reduction(use_reduction)
    solver.init(values, [capacities], [max_capacity])
    computed_value = solver.solve()

    ret = CTestResults()
    ret.total_value = computed_value
    ret.max_capacity = max_capacity
    for i in range(len(values)):
        if solver.best_solution_contains(i):
            ret.packed_item_ids.append(ids[i])
            ret.packed_capacities.append(capacities[i])
            ret.packed_values.append(values[i])
            ret.total_capacity += capacities[i]
    
    return ret