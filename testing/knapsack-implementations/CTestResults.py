class CTestResults():

    def __init__(self) -> None:
        self.elapsed_time   = 0
        self.max_capacity   = 0
        self.total_capacity = 0
        self.total_value    = 0
        
        self.packed_item_ids    = []
        self.packed_capacities  = []
        self.packed_values      = []
    
    def __str__(self) -> str:
        return (f"Elapsed time [sec]: {self.elapsed_time}\n" +
                f"Max possible capacity: {self.max_capacity}\n" +
                f"Used capacity: {self.total_capacity}\n" +
                f"Total value: {self.total_value}\n" +
                f"Packed items: {self.packed_item_ids}\n" +
                f"Packed capacities: {self.packed_capacities}\n" +
                f"Packed values: {self.packed_values}\n")