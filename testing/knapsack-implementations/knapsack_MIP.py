import os, sys
import mip

# append path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)))))
from EOptimMethod import *
from CTestResults import *

def solve_knapsack(values, capacities, ids, max_capacity, optim_method = EOptimMethod.REGULAR) -> CTestResults:    
    tmp_ids = range(len(values))
    model = mip.Model("Knapsack", solver_name=mip.CBC)
    model.verbose = 0 # supress verbose optimization and settings output

    x = [model.add_var(var_type=mip.BINARY) for i in tmp_ids]
    model.objective = mip.maximize(mip.xsum(values[i] * x[i] for i in tmp_ids))
    model += mip.xsum(capacities[i] * x[i] for i in tmp_ids) <= max_capacity

    model.optimize()
    if model.status != mip.OptimizationStatus.OPTIMAL:
        print("!!! WARNING: Optimal solution not found !!!")
    selected = [i for i in tmp_ids if x[i].x >= 0.99]

    ret = CTestResults()
    ret.total_value = model.objective_value
    ret.max_capacity = max_capacity
    for i in selected:
        ret.packed_item_ids.append(ids[i])
        ret.packed_capacities.append(capacities[i])
        ret.packed_values.append(values[i])
        ret.total_capacity += capacities[i]
    
    return ret