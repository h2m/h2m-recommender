import os, sys
import gurobipy as gp
from gurobipy import GRB

# append path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)))))
from EOptimMethod import *
from CTestResults import *

def solve_knapsack(values, capacities, ids, max_capacity, optim_method = EOptimMethod.REGULAR) -> CTestResults:    
    N = len(values)
    model = gp.Model("Knapsack")
    model.Params.OutputFlag = 0 # supress verbose optimization and settings output
    x = model.addVars(N, vtype=GRB.BINARY, name='x')
    obj_fcn = sum(values[i] * x[i] for i in range(N))
    model.setObjective(obj_fcn, GRB.MAXIMIZE)
    model.addConstr(sum(capacities[i] * x[i] for i in range(N)) <= max_capacity)
    
    if optim_method == EOptimMethod.BRANCH_AND_BOUND:
        # Branch and bound method (Barrier)
        model.Params.Method = 2
    elif optim_method == EOptimMethod.BRANCH_AND_BOUND2:
        # Branch and bound settings from https://support.gurobi.com/hc/en-us/community/posts/4404024478737-How-to-program-a-pure-Branch-Bound
        model.Params.Cuts = 0
        model.Params.Heuristics = 0
        model.Params.RINS = 0
        model.Params.Presolve = 0
        model.Params.Aggregate = 0
        model.Params.Symmetry = 0
        model.Params.Disconnected = 0

    model.optimize()

    if model.status != GRB.OPTIMAL:
        print("!!! WARNING: Optimal solution not found !!!")

    ret = CTestResults()
    ret.total_value = model.objVal
    ret.max_capacity = max_capacity
    res_vars = model.getVars()
    for i in range(len(values)):
        if res_vars[i].x == 1:
            ret.packed_item_ids.append(ids[i])
            ret.packed_capacities.append(capacities[i])
            ret.packed_values.append(values[i])
            ret.total_capacity += capacities[i]
    
    return ret