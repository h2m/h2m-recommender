from enum import Enum

class EOptimMethod(Enum):
    REGULAR = 1
    BRANCH_AND_BOUND = 2
    BRANCH_AND_BOUND2 = 3
    CBC = 4