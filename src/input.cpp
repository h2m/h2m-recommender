#include "input.h"

// Input Paramaters
InputParameters::InputParameters() {}; 
int InputParameters::ParseArguments(char* argv[]) {
    // Separator for a serie within an argument (500,1000,1500)
    const char separator[2] = ",";

    // Get type of analyze
    try {
        aggregation_ = std::stoul(argv[1]);
    }
    catch (const std::invalid_argument& ia) {
        std::cerr << "Error with Aggregation argument" << std::endl;
        std::cerr << "Arguments must be an integer" << std::endl;
        std::cerr << "Invalid argument: " << ia.what() << '\n';
        return EXIT_FAILURE;
    }
    try {
        if(aggregation_ > 3) throw (aggregation_);
    }
    catch (unsigned int arg_to_test) {
        std::cerr << "Error with Aggregation argument" << std::endl;
        std::cerr << "Arguments must 1,2,3" << std::endl;
        std::cerr << "Arguments = " << arg_to_test << std::endl;
        return EXIT_FAILURE;
    }

    // Get capacity limit to test
    char* token = strtok(argv[2], separator);
    try {
        while( token != NULL ) {
            cap_limit_.push_back(std::stoul(token));
            token = strtok(NULL, separator);
        }
    }
    catch (const std::invalid_argument& ia) {
        std::cerr << "Error with capacity limit argument" << std::endl;
        std::cerr << "Arguments must be a list of integer in Mb separated by a coma. e.g 500,1000,1500" << std::endl;
        std::cerr << "Invalid argument: " << ia.what() << '\n';
        return EXIT_FAILURE;
    }

    // Get number of threads
    try {
        n_thread_ = std::stoul(argv[3]);
    }
    catch (const std::invalid_argument& ia) {
        std::cerr << "Error with Nb of threads argument" << std::endl;
        std::cerr << "Arguments must be an integer" << std::endl;
        std::cerr << "Invalid argument: " << ia.what() << '\n';
        return EXIT_FAILURE;
    }
    try {
        if(n_thread_ < 1) throw (n_thread_);
    }
    catch (unsigned int arg_to_test) {
        std::cerr << "Error with Nb of threads argument" << std::endl;
        std::cerr << "Arguments must be different higher than 0" << std::endl;
        std::cerr << "Arguments = " << arg_to_test << std::endl;
        return EXIT_FAILURE;
    }

    // Get performance model function
    token = strtok(argv[4], separator);
    try {
        while( token != NULL ) {
            if (std::string(token) != "GAIN" && std::string(token) != "GAIN_AND_TRANSFER") throw token;
            else { 
                test_function_.push_back(token);
                token = strtok(NULL, separator);
            }
        }
    }
    catch (char* char_to_test ) {
        std::cerr << "Error with function-to-test argument" << std::endl;
        std::cerr << "Arguments must be GAIN or GAIN_AND_TRANSFER. Separated by a coma if multiple: GAIN,GAIN_AND_TRANSFER" << std::endl;
        std::cerr << "Invalid argument: " << char_to_test << '\n';
        return EXIT_FAILURE;
    }

    // Get weigthing factor for phase based approach
    try {
        pm_weighting_factor_ = std::stoul(argv[5]);;
    }
    catch (const std::invalid_argument& ia) {
        std::cerr << "Error with Phase Based Weighting factor argument" << std::endl;
        std::cerr << "Arguments must be an integer" << std::endl;
        std::cerr << "Invalid argument: " << ia.what() << '\n';
        return EXIT_FAILURE;
    }
    try {
        if(pm_weighting_factor_ < 1) throw (pm_weighting_factor_);
    }
    catch (unsigned int arg_to_test) {
        std::cerr << "Error with Phase Based Weighting factor argument" << std::endl;
        std::cerr << "Arguments must be different higher than 0" << std::endl;
        std::cerr << "Arguments = " << arg_to_test << std::endl;
        return EXIT_FAILURE;
    }

    // Get the sampling frequency
    try {
        sampling_frequency_ = std::stoul(argv[6]);
    }
    catch (const std::invalid_argument& ia) {
        std::cerr << "Error with NumaMMA Sampling frequency argument" << std::endl;
        std::cerr << "Arguments must be an integer" << std::endl;
        std::cerr << "Invalid argument: " << ia.what() << '\n';
        return EXIT_FAILURE;
    }
    try {
        if(sampling_frequency_ < 1) throw (sampling_frequency_);
    }
    catch (unsigned int arg_to_test) {
        std::cerr << "Error with NumaMMA Sampling frequency argument" << std::endl;
        std::cerr << "Arguments must be different higher than 0" << std::endl;
        std::cerr << "Arguments = " << arg_to_test << std::endl;
        return EXIT_FAILURE;
    }

    // Get performance model function
    token = strtok(argv[7], separator);
    try {
        while( token != NULL ) {
            if (std::stoi(token) < 0) throw token;
            else {
                nb_phases_.push_back(std::stoul(token));
                token = strtok(NULL, separator);
            }
        }
    }
    catch (const std::invalid_argument& ia) {
        std::cerr << "Error with Number of phases argument" << std::endl;
        std::cerr << "Arguments must be a list of integer separated by a coma. e.g 0,1,2,3" << std::endl;
        std::cerr << "Invalid argument: " << ia.what() << '\n';
        return EXIT_FAILURE;
    }
    catch (char * token) {
        std::cerr << "Error with Number of phases argument" << std::endl;
        std::cerr << "Arguments must be a list of non negative integer separated by a coma. e.g 0,1,2,3" << std::endl;
        std::cerr << "Invalid argument: " << token << '\n';
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
};

// NumaMMA Objects
NumammaMemoryObject::NumammaMemoryObject() {} // empty constructor
NumammaMemoryObject::NumammaMemoryObject(const char* filename) {

    std::ifstream InFile(filename);
    if (!InFile.good())
    {
        std::cout << "Error: Could not load all_memory_objects.dat" << std::endl;
        std::cout << "Check path, currently here: " << std::endl;
        std::cout << filename << std::endl;
        exit(1);
    }

    std::string line {};
    // tmp for filtering before pushing back to object.
    std::vector<std::string> tmp_adress, tmp_callstack_rip, tmp_callstack_offsets, tmp_callsite_rip, tmp_callsite;
    std::vector<unsigned int> tmp_id, tmp_size, tmp_alloc_date, tmp_dealoc_date;

    std::getline(InFile, line); // skip header.
    while (std::getline(InFile, line))
    {
        std::stringstream iss(line);
        std::string id {1,42}, adress {1,42}, size {1,42}, alloc {1,42}, dealloc {1,42};
        std::string callstack_rip {1,42}, callstack_offset {1,42}, callsite_rip {1,42}, callsite{1,42};

        // filter out object on stack or having NULL in file.
        if (!((line.find("NULL") != -1) || (line.find("[stack]") != -1))) {
            
            std::getline(iss, id, '\t');
            std::getline(iss, adress, '\t');
            std::getline(iss, size, '\t');
            std::getline(iss, alloc, '\t');
            std::getline(iss, dealloc, '\t');
            std::getline(iss, callstack_rip, '\t');
            std::getline(iss, callstack_offset, '\t');
            std::getline(iss, callsite_rip, '\t');
            std::getline(iss, callsite, '\t');
        // if (std::stoi(id)  == 98) std::cout << line << std::endl;

            // find callsite only made of 0x0. (cannot be done on a full line.)
            if (callsite_rip.find_last_of("0x0") != -1) {

                tmp_id.push_back(std::stoi(id));
                tmp_adress.push_back(adress);
                tmp_size.push_back(std::stoul(size));
                tmp_alloc_date.push_back(std::stoul(alloc));
                tmp_dealoc_date.push_back(std::stoul(dealloc));
                tmp_callstack_rip.push_back(callstack_rip);
                tmp_callstack_offsets.push_back(callstack_offset);
                tmp_callsite_rip.push_back(callsite_rip);
                tmp_callsite.push_back(callsite);
            }
        }
    }
    // for(int i = 0; i < tmp_id.size(); i++) {
    //     if (tmp_id[i]  == 98) std::cout << "found you" << std::endl;
    // }

    // Removing redundant objects from file.
    // Find redaundant ids
    std::vector<int> redundant(tmp_id.size(),-1);
    for (int i = 0; i < tmp_id.size(); i++) {
        unsigned int id_to_test = tmp_id[i];
        for (int j = 0; j < tmp_id.size(); j++) {
            if (id_to_test == tmp_id[j]) {
                if (i<j) redundant[i] = j;
            }
        }
    }
    // Actually remove them
    auto it = std::find(redundant.begin(),redundant.end(), -1);
    redundant.erase(it, redundant.end());

    for(auto i : redundant) {
        id_.push_back(tmp_id[i]);
        address_.push_back(tmp_adress[i]);
        size_.push_back(tmp_size[i]);
        alloc_date_.push_back(tmp_alloc_date[i]);
        dealloc_date_.push_back(tmp_dealoc_date[i]);
        callstack_rip_.push_back(tmp_callstack_rip[i]);
        callstack_offsets_.push_back(tmp_callstack_offsets[i]);
        callsite_rip_.push_back(tmp_callsite_rip[i]);
        callsite_.push_back(tmp_callsite[i]);
    }

    InFile.close();

};

// Phases defininition
H2mPhase::H2mPhase(const char* filename) {

    std::ifstream InFile(filename);
    if (!InFile.good())
    {
        std::cout << "Error: Could not load h2m_dump_phase_transitions.csv" << std::endl;
        std::cout << "Check path, currently here: " << std::endl;
        std::cout << filename << std::endl;
        exit(1);
    }
    std::string line {};
    std::getline(InFile, line); // skip header.

    while (std::getline(InFile, line))
    {
        std::stringstream iss(line);
        std::string id {1,42}, name {1,42}, time {1,42};
        std::getline(iss, id, ';');
        std::getline(iss, name, ';');
        std::getline(iss, time, ';');

        id_.push_back(std::stoul(id));
        name_.push_back(name);
        time_stamp_.push_back(std::stoul(time));

    }
    InFile.close();

}

// NumaMMA Accesses
NumammaMemoryAccess::NumammaMemoryAccess(const char* filename) {

    std::ifstream InFile(filename);
    if (!InFile.good())
    {
        std::cout << "Error: Could not load all_memory_accesses.dat" << std::endl;
        std::cout << "Check path, currently here: " << std::endl;
        std::cout << filename << std::endl;
        exit(1);
    }
    std::string line {};
    std::getline(InFile, line); // skip header.

    while (std::getline(InFile, line))
    {
        std::stringstream iss(line);
        std::string thread_rank {1,42}, time_stamp {1,42}, object_id {1,42};
        std::string offset {1,42}, mem_level {1,42}, access_weight {1,42}, access_type {1,42};
        bool is_write {false};

        std::getline(iss, thread_rank, ' ');
        std::getline(iss, time_stamp, ' ');
        std::getline(iss, object_id, ' ');
        std::getline(iss, offset, ' ');
        std::getline(iss, mem_level, ' ');
        std::getline(iss, access_weight, ' ');
        std::getline(iss, access_type, ' ');

        if (access_type == "w") is_write = 1;
        
        // HERE YOU CAN FILTER OUT VALUES!!
        thread_rank_.push_back(std::stoul(thread_rank));
        time_stamp_.push_back(std::stoul(time_stamp)); // CAUTION, time is in nano second
        object_id_.push_back(std::stoul(object_id));
        offset_.push_back(std::stoul(offset));
        mem_level_.push_back(mem_level);
        access_weight_.push_back(std::stoul(access_weight));
        is_write_.push_back(is_write);
    }
    InFile.close();
};

// Allocation (Objects + Accesses)
NumammaAllocation::NumammaAllocation() {};

NumammaAllocation::NumammaAllocation(const NumammaMemoryObject& Obj, const NumammaMemoryAccess& Acc ) {

    for (int i = 0; i < Obj.id_.size(); i++) {
        int count = std::count(Acc.object_id_.begin(), Acc.object_id_.end(), Obj.id_[i]);

        Access tmpAcc;
        // Arbitrary, keep object that are at least called 20 times
        if (count>20) {
            id_.push_back(Obj.id_[i]);
            adress_.push_back(Obj.address_[i]);
            size_.push_back(Obj.size_[i]);
            alloc_date_.push_back(Obj.alloc_date_[i]);
            dealoc_date_.push_back(Obj.dealloc_date_[i]);
            callstack_rip_.push_back(Obj.callstack_rip_[i]);
            callstack_offsets_.push_back(Obj.callstack_offsets_[i]);
            callsite_rip_.push_back(Obj.callsite_rip_[i]);
            callsite_.push_back(Obj.callsite_[i]);
        
            for (int j = 0; j < Acc.object_id_.size(); j++) {
                // if an object is not accessed, we dont take into account.
                if (Obj.id_[i] == Acc.object_id_[j]) { 
                    tmpAcc.access_weight_.push_back(Acc.access_weight_[j]);
                    tmpAcc.is_write_.push_back(Acc.is_write_[j]);
                    tmpAcc.mem_level_.push_back(Acc.mem_level_[j]);
                    tmpAcc.offset_.push_back(Acc.offset_[j]);
                    tmpAcc.time_stamp_.push_back(Acc.time_stamp_[j]);
                    tmpAcc.thread_rank_.push_back(Acc.thread_rank_[j]);
                    tmpAcc.id_.push_back(Acc.object_id_[j]);
                }
            }
            access_.push_back(tmpAcc);
        }
    }
};

int NumammaAllocation::SaveAsCSVFile(const char* filename) {
    
    std::ofstream OutFile(filename);
    if (!OutFile.good())
    {
        std::cout << "Error: Could not allocate Allocation.dat" << std::endl;
        std::cout << "Check path, currently here: " << std::endl;
        std::cout << filename << std::endl;
        exit(1);
    }

    OutFile << "id,size,alloc_date,dealloc_date,adress,callstack_rip,callstack_offset,callsite_rip,callsite,nb_access" << std::endl;
    for (int i = 0; i < id_.size(); i++) {
        OutFile << id_[i] << ";" << size_[i] << ";" << alloc_date_[i] << ";" << dealoc_date_[i] << ";" << adress_[i] << ";"
        << callstack_rip_[i] << ";" << callstack_offsets_[i] << ";" << callsite_rip_[i] << ";" << callsite_[i] << ";" << access_[i].id_.size() << std::endl;
        for (int j = 0; j < access_[i].id_.size(); j++) {
            OutFile << access_[i].id_[j] << ";" << access_[i].thread_rank_[j] << ";" << access_[i].time_stamp_[j] << ";" 
            << access_[i].offset_[j] << ";" << access_[i].mem_level_[j] << ";" << access_[i].access_weight_[j] << ";" << access_[i].is_write_[j] << std::endl; 
        } 
    }
    OutFile.close();
    return 0;
};

int NumammaAllocation::LoadFromCSVFile(const char* filename) {

    std::ifstream InFile(filename);
    if (!InFile.good())
    {
        std::cout << "Error: Could not load csv file" << std::endl;
        std::cout << "Check path, currently here: " << std::endl;
        std::cout << filename << std::endl;
        exit(1);
    }

    std::string line {};
    std::getline(InFile, line); // skip header.
    while (std::getline(InFile, line))
    /* The file is structured by block of ojects:
    id,size,alloc_date,dealloc_date,adress,callstack_rip,callstack_offset,callsite_rip,callsite, nb_acess
    id,thread_rank,time_stamp,offset,mem_level,access_weight,is_write
    -
    - nb_acess lines
    -
    -  
    id,size,alloc_date,dealloc_date,adress,callstack_rip,callstack_offset,callsite_rip,callsite
    id,thread_rank,time_stamp,offset,mem_level,access_weight,is_write
    -
    -
    - nb_access lines
    -
    */
    {
        std::stringstream iss(line);

        // Object related
        std::string id {1,42}, adress {1,42}, size {1,42}, alloc {1,42}, dealloc {1,42}, tmp_nb_access {1,42};
        std::string callstack_rip {1,42}, callstack_offset {1,42}, callsite_rip {1,42}, callsite{1,42};
        Access Acc;

        // bool is_write {false};

        std::getline(iss, id, ';');
        std::getline(iss, size, ';');
        std::getline(iss, alloc, ';');
        std::getline(iss, dealloc, ';');
        std::getline(iss, adress, ';');
        std::getline(iss, callstack_rip, ';');
        std::getline(iss, callstack_offset, ';');
        std::getline(iss, callsite_rip, ';');
        std::getline(iss, callsite, ';');
        std::getline(iss, tmp_nb_access, ';');

        unsigned int nb_access = std::stoul(tmp_nb_access);
        for (int i = 0; i < nb_access; i++) {

            std::getline(InFile, line);
            std::stringstream iss(line);
            std::string access_id {1,42}, thread_rank {1,42}, time_stamp {1,42};
            std::string offset {1,42}, mem_level {1,42}, access_weight {1,42}, tmp_is_write {1,42};
            bool is_write {false};

            std::getline(iss, access_id, ';');
            std::getline(iss, thread_rank, ';');
            std::getline(iss, time_stamp, ';');
            std::getline(iss, offset, ';');
            std::getline(iss, mem_level, ';');
            std::getline(iss, access_weight, ';');
            std::getline(iss, tmp_is_write, ';');
            
            if (tmp_is_write == "1") is_write = true;

            Acc.access_weight_.push_back(std::stoul(access_weight));
            Acc.is_write_.push_back(is_write);
            Acc.mem_level_.push_back(mem_level);
            Acc.offset_.push_back(std::stoul(offset));
            Acc.time_stamp_.push_back(std::stoul(time_stamp));
            Acc.thread_rank_.push_back(std::stoul(thread_rank));
            Acc.id_.push_back(std::stoul(access_id));
        }

        // Finally push back everything.
        id_.push_back(std::stoi(id));
        adress_.push_back(adress);
        size_.push_back(std::stoul(size));
        alloc_date_.push_back(std::stoul(alloc));
        dealoc_date_.push_back(std::stoul(dealloc));
        callstack_rip_.push_back(callstack_rip);
        callstack_offsets_.push_back(callstack_offset);
        callsite_rip_.push_back(callsite_rip);
        callsite_.push_back(callsite);
        access_.push_back(Acc);
    }
    
    return 0;
};

MemoryConfig::MemoryConfig() {};
MemoryConfig::MemoryConfig(const char* config_file, const int n_thread) {
    
    std::vector<JsonConfigMemorySpace*> memory_space { };
    std::vector<JsonConfigCopyBandwidth*> copy_bandwidth { };
    
    // Loading json file
    try {
        if (parse_json_config(config_file, memory_space, copy_bandwidth) == EXIT_FAILURE) throw config_file;
    }
    catch(const char* config_file) {
        std::cerr << " Error while reading json config file " << std::endl;
        std::cerr << " json file is supposed to be at " << config_file << std::endl;
        std::exit;
    }
    
    // Model mem_model;
    // Lets select what mem is fast/slow and what we extract from mem content
    for (int i = 0; i < memory_space.size(); i++) {
        if (memory_space[i]->is_mem_space_hbw_) {
            std::cout << "Fast memory is " << memory_space[i]->name_ << std::endl;
            idx_fast_ = memory_space[i]->idx_;
        }
        else idx_slow_ = memory_space[i]->idx_;
    }

    // We now select values related to the number of threads used in our previous experiment.
    bw_read_fast_   = memory_space[idx_fast_]->scaling_bandwidth_read_only_MBs_[n_thread -1];
    bw_read_slow_   = memory_space[idx_slow_]->scaling_bandwidth_read_only_MBs_[n_thread -1];
    bw_write_fast_  = memory_space[idx_fast_]->scaling_bandwidth_read_write_MBs_[n_thread -1];
    bw_write_slow_  = memory_space[idx_slow_]->scaling_bandwidth_read_write_MBs_[n_thread -1];
    lat_read_fast_  = memory_space[idx_fast_]->scaling_latency_read_only_ns_[n_thread -1];
    lat_read_slow_  = memory_space[idx_slow_]->scaling_latency_read_only_ns_[n_thread -1];
    lat_write_fast_ = memory_space[idx_fast_]->scaling_latency_read_write_ns_[n_thread -1];
    lat_write_slow_ = memory_space[idx_slow_]->scaling_latency_read_write_ns_[n_thread -1];
    
    for (int i = 0; i < copy_bandwidth.size(); i++) {
        if(copy_bandwidth[i]->idx_from_ == idx_slow_ && copy_bandwidth[i]->idx_to_ == idx_fast_) {
            // std::cout << "found copy to fast" << std::endl;
            for (int j = 0; j < copy_bandwidth[i]->size_bytes_.size(); j++) {
                copy_to_fast_.byte_size_.push_back(copy_bandwidth[i]->size_bytes_[j]);
                copy_to_fast_.bw_Mbs_.push_back(copy_bandwidth[i]->bandwidth_MBs_[j]);
            }
        } else if(copy_bandwidth[i]->idx_from_ == idx_fast_ && copy_bandwidth[i]->idx_to_ == idx_slow_) {
            // std::cout << "found copy to slow" << std::endl;
            for (int j = 0; j < copy_bandwidth[i]->size_bytes_.size(); j++) {
                copy_to_slow_.byte_size_.push_back(copy_bandwidth[i]->size_bytes_[j]);
                copy_to_slow_.bw_Mbs_.push_back(copy_bandwidth[i]->bandwidth_MBs_[j]);
            }
        }
    }

    // Cleaning what we dont need anymore from the complete config file
    cleanup_json_config(memory_space, copy_bandwidth);

};