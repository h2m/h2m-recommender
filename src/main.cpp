#include "h2m_json.h"
#include "iostream"
#include "fstream"
#include "input.h"
#include "performance_model.h"

#include "omp.h"
//#include "gurobi_c++.h"

int main(const int argc, char* argv[])
{
    if (argc < 7) {
        std::cerr << "Number of arguments must be at least 7" << std::endl;
        return EXIT_FAILURE;
    }
    
    InputParameters inp;
    if (inp.ParseArguments(argv) == EXIT_FAILURE) {
        std::cerr << " Error while reading list of arguments" <<std::endl;
        return EXIT_FAILURE;
    }
    // Variables
    // GRBEnv env = GRBEnv(true);

    // config file
    const char* file_path("/home/pclouzet/Documents/src/h2m-recommender/configs/icelake-optane.json");
    // outputs from numamma
    const char* numamma_file("/home/pclouzet/Documents/src/h2m-recommender/valid_inputs/multi-phase-app/results_numamma_sr_6000/all_memory_objects.dat");
    const char* access_file("/home/pclouzet/Documents/src/h2m-recommender/valid_inputs/multi-phase-app/results_numamma_sr_6000/all_memory_accesses.dat");
    // output from h2m for phase id
    const char* h2m_phase_file("/home/pclouzet/Documents/src/h2m-recommender/valid_inputs/multi-phase-app/results_numamma_sr_6000/h2m_dump_phase_transitions.csv");

    // Loading
    // NumammaMemoryObject numamma_object(numamma_file);
    // NumammaMemoryAccess access(access_file);
    // NumammaAllocation allocations(numamma_object, access);

    NumammaAllocation allocations;
    std::cout << " Allocations done " << std::endl;

    H2mPhase phase(h2m_phase_file);
    std::cout << " Phases done " << std::endl;

    if(allocations.LoadFromCSVFile("/home/pclouzet/Documents/src/h2m-recommender/valid_inputs/multi-phase-app/Allocation.csv") != 0) {
        std::cout << "Error in LoadFromCSVFile" << std::endl;
        return EXIT_FAILURE;
    }
    std::cout << " Allocations loaded " << std::endl;

    MemoryConfig mem_config(file_path, inp.n_thread_);
    std::cout << " Configurations done " << std::endl;

    // if(err = allocations.SaveAsCSVFile("/home/pclouzet/Documents/src/h2m-recommender/valid_inputs/multi-phase-app/Allocation.csv") !=0 ) {
    //     std::cout << "Error in SaveAsCSVFile" << std::endl;
    //     return EXIT_FAILURE;
    // }
    // std::cout << "here" << std::endl;
    Statistics Stats(allocations.id_.size());
    std::cout << " Stastistics done " << std::endl;

    if (Stats.Calculate(allocations,inp.pm_weighting_factor_,mem_config) != 0) {
        std::cout << "Error in Statistic calculation" << std::endl;
        return EXIT_FAILURE;
    };
    std::cout << " Calculation done " << std::endl;

    if (Stats.SaveAsCSVFile(allocations,"/home/pclouzet/Documents/src/h2m-recommender/src/outputs/Statistics.csv") != 0) {
        std::cout << "Error while saving Statistics as CSV file" << std::endl;
        return EXIT_FAILURE; 
    }
    // std::cout << "Nb of allocations = " << allocations.id_.size() << std::endl;


    // Update values read from files, e.g. max memory available for experiments
    // double max_memory_available = 500;
    // for (int i = 0; i < memory_space.size(); i++)
    // {
    //     if (max_memory_available < memory_space[i]->capacity_MB_) {
    //         memory_space[i]->capacity_MB_ = max_memory_available;
    //         std::cout << "Max Memory available = " << memory_space[i]->capacity_MB_ << std::endl;
    //     }
    // }
    // for (int i = 0; i < access.thread_rank_.size(); i++) {
    //     std::cout << " access type = " << access.access_type_[i] << " thread rank "  << access.thread_rank_[i] << std::endl;
    // }
    // Call for Gurobi to solve an optimization knapsack problem.
    // env.set("LogFile","gurobi.log");
    // env.start();

    // Create an empty model
    // GRBModel model = GRBModel(env);
    // GRBVar x = model.addVars();

    // cleaning json file
    // cleanup_json_config(memory_space, copy_bandwidth);
    return 0; 
}
