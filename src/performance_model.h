#ifndef __PERFORMANCE_MODEL_H__
#define __PERFORMANCE_MODEL_H__

#include "input.h"

class Statistics {
    public:
        Statistics(const int nb_objects);
        int Calculate(NumammaAllocation&, unsigned int, MemoryConfig&);
        int SaveAsCSVFile(NumammaAllocation&, const char*);
    public:
        double avg_weight_read_mem_ {0};
        // Statistics
        std::vector<unsigned int> nr_alloc_ ;
        std::vector<double> total_size_Mb_ ;
        std::vector<unsigned int> acc_total_ ;
        std::vector<unsigned int> acc_read_ ;
        std::vector<unsigned int> acc_read_cache_ ;
        std::vector<unsigned int> acc_read_memory_ ;
        std::vector<unsigned int> acc_write_ ;
        std::vector<unsigned int> acc_write_cache_ ;
        std::vector<unsigned int> acc_write_memory_ ;
        std::vector<unsigned int> acc_cache_ ;
        std::vector<unsigned int> acc_memory_ ;
        std::vector<unsigned int> acc_total_phase_ ;
        std::vector<unsigned int> acc_read_phase_ ;
        std::vector<unsigned int> acc_write_phase_ ;
        
        std::vector<double> weight_total_ ;
        std::vector<double> weight_memory_ ;
        std::vector<double> weight_avg_ ;
        std::vector<double> weight_phase_total_ ;
        std::vector<double> weight_phase_memory_ ;

        std::vector<double> pct_read_ ;
        std::vector<double> pct_read_cache_ ;
        std::vector<double> pct_read_memory_ ;
        std::vector<double> pct_read_inphase_ ;
        std::vector<double> pct_write_ ;
        std::vector<double> pct_write_cache_ ;
        std::vector<double> pct_write_memory_ ;
        std::vector<double> pct_write_inphase_ ;
        std::vector<double> pct_acc_cache_ ;
        std::vector<double> pct_acc_memory_ ;
        std::vector<double> pct_acc_inphase_ ;
        std::vector<double> pct_weight_total_ ;
        std::vector<double> pct_weight_memory_ ;
        std::vector<double> pct_weight_self_total_ ;
        std::vector<double> pct_weight_self_memory_ ;
        
        std::vector<double> exp_acc_time_read_fast_part_bw_ ;
        std::vector<double> exp_acc_time_read_fast_part_lat_ ;
        
        std::vector<double> exp_acc_time_read_slow_part_bw_ ;
        std::vector<double> exp_acc_time_read_slow_part_lat_ ;
        
        std::vector<double> exp_acc_time_write_fast_part_bw_ ;
        std::vector<double> exp_acc_time_write_fast_part_lat_ ;
        
        std::vector<double> exp_acc_time_write_slow_part_bw_ ;
        std::vector<double> exp_acc_time_write_slow_part_lat_ ;
        
        std::vector<double> exp_acc_time_read_fast_ ;
        std::vector<double> exp_acc_time_read_slow_ ;
        
        std::vector<double> exp_acc_time_write_fast_ ;
        std::vector<double> exp_acc_time_write_slow_ ;

        std::vector<double> exp_acc_time_fast_ ;
        std::vector<double> exp_acc_time_slow_ ;

        std::vector<double> exp_gain_ ;
        std::vector<double> exp_transfer_time_to_fast_ ;
        std::vector<double> exp_transfer_time_to_slow_ ;

        // header for saving
        std::vector<std::string> header_ = {
            "id",
            "size",
            "min_alloc_date",
            "max_dealloc_date",
            "callstack_rip",
            "callstack_offsets",
            "callsite_rip",
            "callsite",
            "acc_total",
            "acc_read",
            "acc_read_cache",
            "acc_read_memory",
            "acc_write",
            "acc_write_cache",
            "acc_write_memory",
            "acc_cache",
            "acc_memory",
            "acc_total_phase",
            "acc_read_phase",
            "acc_write_phase",
            "weight_total",
            "weight_memory",
            "weight_avg",
            "weight_phase_total",
            "weight_phase_memory",
            "pct_read",
            "pct_read_cache",
            "pct_read_memory",
            "pct_write",
            "pct_write_cache",
            "pct_write_memory",
            "pct_acc_cache",
            "pct_acc_memory",
            "pct_acc_in_phase",
            "pct_reads_in_phase",
            "pct_write_in_phase",
            "pct_weight_total",
            "pct_weight_memory",
            "pct_weight_self_total",
            "pct_weight_self_memory",
            "exp_acc_time_read_fast_part_bw",
            "exp_acc_time_read_fast_part_lat",
            "exp_acc_time_read_slow_part_bw",
            "exp_acc_time_read_slow_part_lat",
            "exp_acc_time_write_fast_part_bw",
            "exp_acc_time_write_fast_part_lat",
            "exp_acc_time_write_slow_part_bw",
            "exp_acc_time_write_slow_part_lat",
            "exp_acc_time_read_fast",
            "exp_acc_time_read_slow",
            "exp_acc_time_write_fast",
            "exp_acc_time_write_slow",
            "exp_acc_time_fast",
            "exp_acc_time_slow",
            "exp_gain",
            "exp_transfer_time_to_fast",
            "exp_transfer_time_to_slow" }; 

};
#endif