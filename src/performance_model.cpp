
#include "performance_model.h"

Statistics::Statistics(const int nb_objects) {
    nr_alloc_.resize(nb_objects,0);
    acc_total_.resize(nb_objects,0);
    acc_read_.resize(nb_objects,0);
    acc_read_cache_.resize(nb_objects,0);
    acc_read_memory_.resize(nb_objects,0);
    acc_write_.resize(nb_objects,0);
    acc_write_cache_.resize(nb_objects,0);
    acc_write_memory_.resize(nb_objects,0);
    acc_cache_.resize(nb_objects,0);
    acc_memory_.resize(nb_objects,0);
    weight_memory_.resize(nb_objects,0);
    weight_total_.resize(nb_objects,0);
    weight_avg_.resize(nb_objects,0);
    weight_phase_memory_.resize(nb_objects,0);
    weight_phase_total_.resize(nb_objects,0);
    pct_read_.resize(nb_objects,0);
    pct_read_cache_.resize(nb_objects,0);
    pct_read_memory_.resize(nb_objects,0);
    pct_write_.resize(nb_objects,0);
    pct_write_cache_.resize(nb_objects,0);
    pct_write_memory_.resize(nb_objects,0);
    pct_acc_cache_.resize(nb_objects,0);
    pct_acc_memory_.resize(nb_objects,0);
    pct_acc_inphase_.resize(nb_objects,0);
    pct_read_inphase_.resize(nb_objects,0);
    pct_write_inphase_.resize(nb_objects,0);
    pct_weight_total_.resize(nb_objects,0);
    pct_weight_memory_.resize(nb_objects,0);
    pct_weight_self_total_.resize(nb_objects,0);
    pct_weight_self_memory_.resize(nb_objects,0);
    acc_total_phase_.resize(nb_objects,0);
    acc_read_phase_.resize(nb_objects,0);
    acc_write_phase_.resize(nb_objects,0);
    exp_acc_time_read_fast_part_bw_.resize(nb_objects,0);
    exp_acc_time_read_fast_part_lat_.resize(nb_objects,0);
    exp_acc_time_read_slow_part_bw_.resize(nb_objects,0);
    exp_acc_time_read_slow_part_lat_.resize(nb_objects,0);
    exp_acc_time_write_fast_part_bw_.resize(nb_objects,0);
    exp_acc_time_write_fast_part_lat_.resize(nb_objects,0);
    exp_acc_time_write_slow_part_bw_.resize(nb_objects,0);
    exp_acc_time_write_slow_part_lat_.resize(nb_objects,0);
    exp_acc_time_read_fast_.resize(nb_objects,0);
    exp_acc_time_read_slow_.resize(nb_objects,0);
    exp_acc_time_write_fast_.resize(nb_objects,0);
    exp_acc_time_write_slow_.resize(nb_objects,0);
    exp_acc_time_fast_.resize(nb_objects,0);
    exp_acc_time_slow_.resize(nb_objects,0);
    exp_gain_.resize(nb_objects,0);
    exp_transfer_time_to_fast_.resize(nb_objects,0);
    exp_transfer_time_to_slow_.resize(nb_objects,0);
};

// A simple function to test if an access was in different layers of cache memory.
inline bool is_access_in_cache_memory_level( std::string& str_to_test, bool read ) {
    if (read) return (str_to_test == "L1_Hit" || str_to_test == "L2_Hit" || str_to_test == "L3_Hit" || str_to_test == "NA");
    else return (str_to_test == "L1_Hit" || str_to_test == "L2_Hit" || str_to_test == "L3_Hit" || str_to_test == "NA");

};

inline double get_bw_for_byte_size(int object_size, MemoryConfig::BWCopy& bw_copy)
{
    int cur_idx = 0;
    for (int i = 0; i < bw_copy.byte_size_.size(); i++) {
        if (bw_copy.byte_size_[i] <= object_size) cur_idx = i;
    }
    return bw_copy.bw_Mbs_[cur_idx];
};


int Statistics::Calculate(NumammaAllocation& Alloc, unsigned int weighting_factor, MemoryConfig& Config) {
    // We dont have write weights so we scale it based on average reads. 
    // And add it once more to the total weights
    // Since the granularity of the aggregation here is the allocated object, 
    // we dont need to loop over another id.
    double weight_costs_bw;
    double weight_costs_lat;

    int sum_weight_read {0}, num_weight {0};
    double tmp_acc_read {0.0}, tmp_acc_write{0.0};
    double scaling_factor_bw_read {0.0}, scaling_factor_bw_write {0.0}; 
    double scaling_factor_lat_read {0.0}, scaling_factor_lat_write {0.0}; 
    double tmp_weight_writes_mem {0.0};

    for (int i = 0; i < Alloc.id_.size(); i++) {
        acc_total_[i] = Alloc.access_[i].time_stamp_.size();

        // We need to do reads first to get all accesses to use them for read counts
        for (int j = 0; j < Alloc.access_[i].id_.size(); j++) {
            weight_total_[i] += Alloc.access_[i].access_weight_[j];

            // Only read counts
            if (!Alloc.access_[i].is_write_[j]) {
                acc_read_[i]++;
                if (is_access_in_cache_memory_level(Alloc.access_[i].mem_level_[j], true)) {
                    acc_read_cache_[i]++;

                    // Calculate mean weight considering all read accesses 
                    if (Alloc.access_[i].access_weight_[j] > 0) {
                        sum_weight_read+=Alloc.access_[i].access_weight_[j];
                        num_weight++;
                    }
                }
                else {
                    weight_memory_[i] += Alloc.access_[i].access_weight_[j];
                }
            }
        }
        tmp_acc_read  = double(acc_read_[i]); // rather allocate once than apply 3 casts
        acc_read_memory_[i]  = acc_read_[i] - acc_read_cache_[i];
        if (acc_read_cache_[i] > 0) weight_avg_[i] = weight_total_[i] / acc_read_cache_[i];
        // percentage values
        if (acc_total_[i] > 0) pct_read_[i] = tmp_acc_read / acc_total_[i];
        if (acc_read_[i] > 0) {
            pct_read_cache_[i] = acc_read_cache_[i] / tmp_acc_read;
            pct_read_memory_[i] = acc_read_memory_[i] / tmp_acc_read;
        }
    }

    // Get average weight mean for write scaling
    if (num_weight != 0) avg_weight_read_mem_ = double(sum_weight_read) / num_weight;

    // Loop for writes and performance model
    for (int i = 0; i < Alloc.id_.size(); i++) {
        acc_write_[i]      = 0;
        acc_write_cache_[i]= 0;
        
        for (int j = 0; j < Alloc.access_[i].id_.size(); j++) {
            if (Alloc.access_[i].is_write_[j]) {
                acc_write_[i]++;
                acc_write_cache_[i] += is_access_in_cache_memory_level(Alloc.access_[i].mem_level_[j], false);
            }
        }

        acc_write_memory_[i] = acc_write_[i] - acc_write_cache_[i];
        acc_cache_[i]        = acc_read_cache_[i] + acc_write_cache_[i];
        acc_memory_[i]       = acc_read_memory_[i] + acc_write_memory_[i];

        tmp_weight_writes_mem = acc_write_memory_[i] * avg_weight_read_mem_;
        weight_total_[i] += tmp_weight_writes_mem;
        weight_memory_[i] += tmp_weight_writes_mem;
        weight_phase_total_[i] = weight_total_[i];
        weight_phase_memory_[i] = weight_memory_[i];

        // percentage values
        tmp_acc_write  = double(acc_write_[i]); // rather allocate once than apply 3 casts
        if (acc_total_[i] > 0) pct_write_[i] = tmp_acc_write / acc_total_[i];
        if (acc_write_[i] > 0) {
            pct_write_cache_[i] = acc_write_cache_[i] / tmp_acc_write;
            pct_write_memory_[i] = acc_write_memory_[i] / tmp_acc_write;
        }
        if (weight_phase_total_[i] > 0) {
            pct_weight_total_[i] = weight_total_[i] / weight_phase_total_[i];
            pct_weight_memory_[i] = weight_memory_[i] / weight_phase_total_[i];
        }

        // Performance model stats:
        scaling_factor_bw_read  = pct_read_cache_[i];
        scaling_factor_lat_read = 1.0 - scaling_factor_bw_read;
        
        // currently use also read cache hit ratio here because SPR system does not get that info for STORE operations.
        scaling_factor_bw_write = pct_read_cache_[i]; 
        scaling_factor_lat_write= 1.0 - scaling_factor_bw_write;

        weight_costs_bw  = weighting_factor;
        weight_costs_lat = weighting_factor;

        // Apply bandwidth costs for every access and scale latency costs 
        exp_acc_time_read_fast_part_bw_[i]   = acc_read_[i]  * 64.0 / Config.bw_read_fast_ / 1e6   * weight_costs_bw;
        exp_acc_time_read_fast_part_lat_[i]  = acc_read_[i]  * Config.lat_read_fast_ / 1e9 / 8.0   * scaling_factor_lat_read * weight_costs_lat;
        exp_acc_time_read_slow_part_bw_[i]   = acc_read_[i]  * 64.0 / Config.bw_read_slow_ / 1e6   * weight_costs_bw;
        exp_acc_time_read_slow_part_lat_[i]  = acc_read_[i]  * Config.lat_read_slow_ / 1e9 / 8.0   * scaling_factor_lat_read * weight_costs_lat;
        exp_acc_time_write_fast_part_bw_[i]  = acc_write_[i] * 64.0 / Config.bw_write_fast_ / 1e6  * weight_costs_bw;
        exp_acc_time_write_fast_part_lat_[i] = acc_write_[i] * Config.lat_write_fast_ / 1e9 / 8.0  * scaling_factor_lat_write * weight_costs_lat;
        exp_acc_time_write_slow_part_bw_[i]  = acc_write_[i] * 64.0 / Config.bw_write_slow_ / 1e6  * weight_costs_bw;
        exp_acc_time_write_slow_part_lat_[i] = acc_write_[i] * Config.lat_write_slow_ / 1e9 / 8.0  * scaling_factor_lat_write * weight_costs_lat;

        exp_acc_time_read_fast_[i]    = exp_acc_time_read_fast_part_bw_[i]  + exp_acc_time_read_fast_part_lat_[i];
        exp_acc_time_read_slow_[i]    = exp_acc_time_read_slow_part_bw_[i]  + exp_acc_time_read_slow_part_lat_[i];
        exp_acc_time_write_fast_[i]   = exp_acc_time_write_fast_part_bw_[i] + exp_acc_time_write_fast_part_lat_[i];
        exp_acc_time_write_slow_[i]   = exp_acc_time_write_slow_part_bw_[i] + exp_acc_time_write_slow_part_lat_[i];

        exp_acc_time_fast_[i]         = exp_acc_time_read_fast_[i] + exp_acc_time_write_fast_[i];
        exp_acc_time_slow_[i]         = exp_acc_time_read_slow_[i] + exp_acc_time_write_slow_[i];
        exp_gain_[i]                  = exp_acc_time_slow_[i]      - exp_acc_time_fast_[i];
        
        // get_bw_for_byte_size(total_size_Mb_[i], Config.copy_to_fast_) 
        exp_transfer_time_to_fast_[i] = double(Alloc.size_[i]) / (1e6 * get_bw_for_byte_size(Alloc.size_[i], Config.copy_to_fast_));
        exp_transfer_time_to_slow_[i] = double(Alloc.size_[i]) / (1e6 * get_bw_for_byte_size(Alloc.size_[i], Config.copy_to_slow_));

    }
    return 0;
};

int Statistics::SaveAsCSVFile(NumammaAllocation& Alloc, const char* filename) {
    for (int i = 0; i < Alloc.id_.size(); i++) {
    } 
    std::ofstream OutFile(filename);
    if (!OutFile.good())
    {
        std::cout << "Error: Could not allocate Statistics.csv" << std::endl;
        std::cout << "Check path, currently here: " << std::endl;
        std::cout << filename << std::endl;
        exit(1);
    }

    for (int i = 0; i < header_.size(); i++) {
        OutFile << header_[i] << ";";
    }
    OutFile << std::endl;

    for (int i = 0; i < Alloc.id_.size(); i++) {
        OutFile 
        << Alloc.id_[i] << ";" 
        << Alloc.size_[i] << ";" 
        << Alloc.alloc_date_[i] << ";" 
        << Alloc.dealoc_date_[i] << ";" 
        << Alloc.callstack_rip_[i] << ";" 
        << Alloc.callstack_offsets_[i] << ";" 
        << Alloc.callsite_rip_[i] << ";" 
        << Alloc.callsite_[i] << ";" 
        << acc_total_[i] << ";" 
        << acc_read_[i] << ";" 
        << acc_read_cache_[i] << ";" 
        << acc_read_memory_[i] << ";" 
        << acc_write_[i] << ";" 
        << acc_write_cache_[i] << ";"
        << acc_write_memory_[i] << ";" 
        << acc_cache_[i] << ";" 
        << acc_memory_[i] << ";" 
        << acc_total_phase_[i] << ";" 
        << acc_read_phase_[i] << ";" 
        << acc_write_phase_[i] << ";"
        << weight_total_[i] << ";" 
        << weight_memory_[i] << ";" 
        << weight_avg_[i] << ";" 
        << weight_phase_total_[i] << ";" 
        << weight_phase_memory_[i] << ";"
        << pct_read_[i] << ";" 
        << pct_read_cache_[i] << ";" 
        << pct_read_memory_[i] << ";" 
        << pct_write_[i] << ";" 
        << pct_write_cache_[i] << ";" 
        << pct_write_memory_[i] << ";"
        << pct_acc_cache_[i] << ";" 
        << pct_acc_memory_[i] << ";" 
        << pct_acc_inphase_[i] << ";" 
        << pct_read_inphase_[i] << ";" 
        << pct_write_inphase_[i] << ";"
        << pct_weight_total_[i] << ";" 
        << pct_weight_memory_[i] << ";" 
        << pct_weight_self_total_[i] << ";" 
        << pct_weight_self_memory_[i] << ";"
        << exp_acc_time_read_fast_part_bw_[i] << ";" 
        << exp_acc_time_read_fast_part_lat_[i] << ";" 
        << exp_acc_time_read_slow_part_bw_[i] << ";" 
        << exp_acc_time_read_slow_part_lat_[i] << ";"
        << exp_acc_time_write_fast_part_bw_[i] << ";" 
        << exp_acc_time_write_fast_part_lat_[i] << ";" 
        << exp_acc_time_write_slow_part_bw_[i] << ";" 
        << exp_acc_time_write_slow_part_lat_[i] << ";"
        << exp_acc_time_read_fast_[i] << ";" 
        << exp_acc_time_read_slow_[i] << ";" 
        << exp_acc_time_write_fast_[i] << ";" 
        << exp_acc_time_write_slow_[i] << ";"
        << exp_acc_time_fast_[i] << ";" 
        << exp_acc_time_slow_[i] << ";" 
        << exp_gain_[i] << ";" 
        << exp_transfer_time_to_fast_[i] << ";" 
        << exp_transfer_time_to_slow_[i] << ";"
        << std::endl; 
    }
    OutFile.close();
    return 0;
};