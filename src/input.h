#ifndef __INPUT_H__
#define __INPUT_H__

#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <istream>
#include <algorithm>
#include <iterator>
#include <cstring>
#include "h2m_json.h"

class InputParameters {
public:
    InputParameters();
    int ParseArguments(char* argv[]);
public:
    unsigned int aggregation_ {0};
    std::vector<unsigned int> cap_limit_ {};
    unsigned int n_thread_ {0};
    std::vector<std::string> test_function_ {};
    unsigned int pm_weighting_factor_ {1};
    unsigned int sampling_frequency_ {6000};
    std::vector<unsigned int> nb_phases_ {9999};

};

struct Access
{
    std::vector<unsigned int> thread_rank_;
    std::vector<unsigned long int> time_stamp_;
    std::vector<unsigned int> offset_;
    std::vector<std::string> mem_level_;
    std::vector<unsigned int> access_weight_;
    std::vector<bool> is_write_;
    std::vector<unsigned int> id_;
};

class NumammaMemoryObject {

    public:
    NumammaMemoryObject();
    NumammaMemoryObject(const char* filename);

    public:
    std::vector<int> id_ ;
    std::vector<unsigned int> size_ ;
    std::vector<unsigned int> alloc_date_ ;
    std::vector<unsigned int> dealloc_date_ ;
    std::vector<std::string> address_ ;
    std::vector<std::string> callstack_rip_ ;
    std::vector<std::string> callstack_offsets_ ;
    std::vector<std::string> callsite_rip_ ;
    std::vector<std::string> callsite_ ; 
    
};

class H2mPhase {
    public:
        H2mPhase(const char* filename);
        // ~H2mPhase();
    public:
        std::vector<unsigned int> id_;
        std::vector<std::string> name_;
        std::vector<unsigned long int> time_stamp_;
    
};


class NumammaMemoryAccess {
    public:
        NumammaMemoryAccess(const char* filename);
    public:
        std::vector<unsigned int> thread_rank_;
        std::vector<unsigned long int> time_stamp_;
        std::vector<unsigned int> object_id_;
        std::vector<unsigned int> offset_;
        std::vector<std::string> mem_level_;
        std::vector<unsigned int> access_weight_;
        std::vector<bool> is_write_;
};

class NumammaAllocation {
    public:
        NumammaAllocation();
        NumammaAllocation( const NumammaMemoryObject&, const NumammaMemoryAccess&);
        // void CalculateStats();
        int SaveAsCSVFile(const char*);
        int LoadFromCSVFile(const char* );
    
    public:
        // NumaMMA specific (object.dat) 
        std::vector<int> id_ ;
        std::vector<unsigned int> size_ ; // Read as bytes
        std::vector<unsigned int> alloc_date_ ;
        std::vector<unsigned int> dealoc_date_ ;
        std::vector<std::string> adress_ ;
        std::vector<std::string> callstack_rip_ ;
        std::vector<std::string> callstack_offsets_ ;
        std::vector<std::string> callsite_rip_ ;
        std::vector<std::string> callsite_ ; 
        // (access.dat)
        std::vector<Access> access_;
};

class MemoryConfig {
public:
    MemoryConfig();
    MemoryConfig(const char* config_file, const int n_thread);
public: 
    struct BWCopy {
        std::vector<long int> byte_size_{}; // bytes
        std::vector<double> bw_Mbs_{}; // Mbytes/s
    };

    int idx_fast_{0}, idx_slow_{0};
    int bw_read_fast_{0}, bw_read_slow_{0};
    int bw_write_fast_{0}, bw_write_slow_{0};
    int lat_read_fast_{0}, lat_read_slow_{0};
    int lat_write_fast_{0}, lat_write_slow_{0};
    BWCopy copy_to_fast_, copy_to_slow_;

};
#endif