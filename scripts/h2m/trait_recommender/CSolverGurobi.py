import gurobipy as gp
from gurobipy import GRB
from timeit import default_timer as timer

class CSolverGurobi():
    @staticmethod
    def solve_knapsack(max_capacity_bytes, array_values, array_weights_bytes,
                       array_alloc_ids, already_allocated, currently_in_fast_mem,
                       cost_slow_to_fast, cost_fast_to_slow, new_obj=True):
        N = len(array_values)

        with gp.Env(empty=True) as env:
            env.setParam('OutputFlag', 0)
            env.setParam('LogToConsole', 0)
            env.start()
            
            start = timer()
            model = gp.Model("Knapsack", env=env)
            x = model.addVars(N, vtype=GRB.BINARY, name='x')
            if new_obj:
                obj_fcn = sum(array_values[i] * x[i] - already_allocated[i] * (((1-currently_in_fast_mem[i])*(x[i]-0)*cost_slow_to_fast[i]) + ((currently_in_fast_mem[i]-0)*(1-x[i])*cost_fast_to_slow[i])) for i in range(N))
            else:
                obj_fcn = sum(array_values[i] * x[i] for i in range(N))
            model.setObjective(obj_fcn, GRB.MAXIMIZE)
            model.addConstr(sum(array_weights_bytes[i] * x[i] for i in range(N)) <= max_capacity_bytes)
            model.Params.Method = 2  # Set to use the branch-and-bound method
            model.optimize()

            packed_items, packed_weights = [], []
            total_weight = 0
            res_vars = model.getVars()
            for i in range(len(array_values)):
                if res_vars[i].x == 1:
                    packed_items.append(array_alloc_ids[i])
                    packed_weights.append(array_weights_bytes[i])
                    total_weight += array_weights_bytes[i]
            end = timer()

            if model.status != GRB.OPTIMAL:
                print("!!! Optimal solution not found !!!")
            obj_val = model.objVal

        return obj_val, total_weight, packed_items, packed_weights