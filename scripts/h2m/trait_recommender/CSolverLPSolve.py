import os
import subprocess

class CSolverLPSolve():
    @staticmethod
    def solve_knapsack(max_capacity_bytes, array_values, array_weights_bytes, array_alloc_ids, already_allocated, currently_in_fast_mem, cost_slow_to_fast, cost_fast_to_slow, new_obj=True):
        path_lp_solve = os.getenv('KS_LPSOLVE_PATH')
        if path_lp_solve is None or not os.path.isfile(path_lp_solve):
            print("ERROR: PATH to LPSolve not set or file not existing. Please set env variable KS_LPSOLVE_PATH")
            exit(2)

        # Step 1: create tmp .lp file with optimization objectives and constraits
        tmp_file = "tmp_file_lpsolve.lp"
        with open(tmp_file, 'w', newline='') as f:
            # determine objective function expression
            obj_expr = ""
            for i in range(len(array_values)):
                tmp_obj = f"{array_values[i]} * x_{i}"
                # only add term if new objective function is used that incorporates transfer times
                if new_obj:
                    # only subtract values if item is already allocated and residing somewhere
                    # if item is allocated in this phase, there are no additional costs
                    if already_allocated[i]:
                        if currently_in_fast_mem[i]:
                            tmp_obj += f" - {cost_fast_to_slow[i]} + {cost_fast_to_slow[i]} * x_{i}"
                        else:
                            tmp_obj += f" - {cost_slow_to_fast[i]} * x_{i}"
                # append expression
                if i == 0:
                    obj_expr = tmp_obj
                else:
                    obj_expr = obj_expr + " + "  + tmp_obj
            
            # write objective function to file
            f.write(f"max: {obj_expr};\n")

            # determine constraints
            constraint_expr = ""
            for i in range(len(array_values)):
                tmp_const = f"{array_weights_bytes[i]} * x_{i}"
                if i == 0:
                    constraint_expr = tmp_const
                else:
                    constraint_expr = constraint_expr + " + "  + tmp_const
            constraint_expr += f" <= {max_capacity_bytes}"
            # write constraints to file
            f.write(f"{constraint_expr};\n")

            # specify that decision variables are binary
            tmp_list = ", ".join([f"x_{x}" for x in range(len(array_values))])
            f.write(f"binary {tmp_list};\n")

        # Step 2: Call lpsolve <file> to generate optimization
        tmp_cmd = [f"{path_lp_solve}", f"{tmp_file}"]
        test_output = subprocess.check_output(tmp_cmd, timeout=120)
        test_output = test_output.decode("UTF-8") # convert to string
        lines = test_output.replace("\r","").split("\n")

        # Step 3: Parse and determine outputs
        # with open(res_path) as f: lines = [x.strip() for x in list(f)]
        objVal = 0
        total_weight = 0
        packed_items = []
        packed_weights = []
        for li in lines:
            if li.startswith("Value of objective function"):
                objVal = float(li.split(":")[1].strip())
                continue
            if li.startswith("x_"):
                tmp_spl = li.split(" ")
                idx = int(tmp_spl[0].strip().split("_")[1].strip())
                is_in = int(tmp_spl[-1].strip())
                if is_in == 1:
                    packed_items.append(array_alloc_ids[idx])
                    packed_weights.append(array_weights_bytes[idx])
                    total_weight += array_weights_bytes[idx]

        return objVal, total_weight, packed_items, packed_weights