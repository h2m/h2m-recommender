
from .CTraitRecommenderBase import *
from ..common import CLogging
from ..common.CAggregationStats_V1 import CAggregationStats_V1
from ..common.CTraitEntry import *
from typing import Tuple

class CTraitRecommender_TopN(CTraitRecommenderBase):

    logger = CLogging.get_my_logger("Recommender_TopN")

    def __init__(self):
        pass

    def get_traits_for_phase(self, map_mem_spaces: dict[ETraitValue, CMemoryKind], phase: CPhase = None) -> dict[CAllocation, list[CTraitEntry]]:
        if phase is None:
            all_acc = self.dc.get_all_accesses()
        else:
            all_acc = self.dc.get_all_accesses_for_phase(phase.id)

        if all_acc.ts_ms.size == 0:
            return None
        
        if phase is None:
            dict_aggr_alloc = self.dc.map_aggregation_allocs
        else:
            dict_aggr_alloc = self.dc.map_phase_accesses[phase.id]
                
        # map with traits per aggregation entity
        al_traits: dict[CAllocation, list[CTraitEntry]] = {}

        # calculate stats for each allocation entity
        list_stats: list[Tuple[CAggregation, CAllocation, CAggregationStats_V1, list[CTraitEntry]]] = []
        for aggr, allocs in dict_aggr_alloc.items():
            for al in allocs:
                tmp_stats = self.instances.inst_aggregation_stats.get_stats(self.cfg, [al], all_acc)
                list_stats.append((aggr, al, tmp_stats, []))
        
        # sort based on prio
        stats_sorted = sorted(list_stats, key = lambda x: x[2].pct_weight_memory, reverse=True)

        for _, al, stat, cur_traits in stats_sorted:
            # start with trying to put everything in faster HBW
            desired_mem_space = ETraitValue.MEM_SPACE_HBW
            
            # TODO: implement proper sensitivity analysis
            if stat.pct_read_cache > 0.75:
                # Note: prefetching seems to work well here. Try to move it to HBW
                desired_mem_space = ETraitValue.MEM_SPACE_HBW
            elif stat.pct_read_cache < 0.25:
                desired_mem_space = ETraitValue.MEM_SPACE_LOW_LAT
            
            # check capacity limit
            ms = map_mem_spaces[desired_mem_space]
            if al.size_mb > ms.tmp_cap_limit_MB:
                # fall back to large cap mem space
                desired_mem_space = ETraitValue.MEM_SPACE_LARGE_CAP
                ms = map_mem_spaces[desired_mem_space]
            
            # finally set trait and substract from capacity
            ms.tmp_cap_limit_MB -= al.size_mb
            cur_traits.append(CTraitEntry("h2m_atk_req_mem_space", ETraitValue.get_str(desired_mem_space), EValueType.ATV))
        
        # sort by allocation timestamp
        stats_sorted = sorted(list_stats, key = lambda x: x[1].date_alloc_ms, reverse=False)
        
        for _, al, _, cur_traits in stats_sorted:
            al_traits[al] = cur_traits

        return al_traits
    
    def generate(self, map_mem_spaces: dict[ETraitValue, CMemoryKind], phase_based: bool = False, respect_lifetime: bool = False) -> dict[CPhase, dict[CAllocation, list[CTraitEntry]]]:
        map_traits: dict[CPhase, dict[CAllocation, list[CTraitEntry]]] = {}

        if phase_based:
            for ph in self.dc.phases:
                # reset capacity for every phase
                for ms in map_mem_spaces.values():
                    ms.tmp_cap_limit_MB = ms.capacity_MB

                # get traits
                tmp_traits = self.get_traits_for_phase(map_mem_spaces, ph)
                if tmp_traits is not None:
                    map_traits[ph] = tmp_traits
        else:
            # reset capacity for every phase
            for ms in map_mem_spaces.values():
                ms.tmp_cap_limit_MB = ms.capacity_MB

            ph = CPhase(-1, "Complete Program", -1, -1)
            map_traits[ph] = self.get_traits_for_phase(map_mem_spaces)

        return map_traits, None