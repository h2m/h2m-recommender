from abc import ABCMeta, abstractmethod
from ..common.CDataTypes import *
from ..common.CManualInstances import CManualInstances
from ..common.CTraitEntry import *
from ..common.CDecisionTrace import CDecisionTrace
from typing import Tuple

class CTraitRecommenderBase(metaclass=ABCMeta):

    # @abstractmethod
    def set_data(self, dc: CDataContainer, cfg: CConfig, mem_info: CMemoryInfo, mi: CManualInstances):
        self.dc             = dc            # reference to current callsite
        self.cfg            = cfg           # make configuration available for recommendation system
        self.instances      = mi
        self.mem_info       = mem_info

    @abstractmethod
    def generate(self, map_mem_spaces: dict[ETraitValue, CMemoryKind], phase_based: bool = False, respect_lifetime: bool = False) -> Tuple[dict[CPhase, dict[CAllocation, list[CTraitEntry]]], CDecisionTrace]:
        # Returns traits and decision trace for each phase and callsite/callstack/allocation
        pass