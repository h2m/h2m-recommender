from enum import unique
import os, sys
from typing import Tuple
import copy

from ..common import CLogging
from ..common.CDataTypes import *
from ..common.CManualInstances import CManualInstances
from ..common.CTraitEntry import ETraitValue
from ..common.CJsonObjects import *

class CTraitRecommendationSystem:
    
    logger = CLogging.get_my_logger("RecommendationSystem")
    dc: CDataContainer
    memory_info: CMemoryInfo
    mi: CManualInstances

    def __init__(self, dc: CDataContainer, cfg: CConfig, memory_info: CMemoryInfo, mi: CManualInstances) -> None:
        self.dc             = dc   # container holding necessary data
        self.cfg            = cfg
        self.memory_info    = memory_info
        self.mi             = mi

    def generate(self, map_mem_spaces: dict[ETraitValue, CMemoryKind]) -> None:
        # dynamically load the desired recommender implementation
        exec(f"from .{self.cfg.class_recommender} import {self.cfg.class_recommender}")
        r = eval(f"{self.cfg.class_recommender}()")
        r.set_data(self.dc, self.cfg, self.memory_info, self.mi)

        # ============================================================
        # == Overall traits for complete program (IDP)
        # ============================================================
        self.logger.info(f"Creating global trait recommendations (IDP) ...")
        map_traits, trace = r.generate(map_mem_spaces, phase_based=False)
        if map_traits is not None:
            cur_name = f"traits_complete-program{self.cfg.dest_file_postfix}"
            # serializing and writing json
            json_object = CJsonTraitsWrapper.generate_json_dump(self.dc, map_traits)
            with open(os.path.join(self.cfg.destination, f"{cur_name}.json"), "w", newline='') as outfile:
                outfile.write(json_object)
            if trace is not None and self.cfg.gen_trace:
                with open(os.path.join(self.cfg.destination, f"{cur_name}_decision_trace.csv"), "w", newline='') as outfile:
                    trace.write_to_file(outfile)

        # ============================================================
        # == Overall traits for complete program (IDP) + respecting lifetime of objects
        # ============================================================
        self.logger.info(f"Creating global trait recommendations (IDP) while respecting object life times ...")
        map_traits, trace = r.generate(map_mem_spaces, phase_based=False, respect_lifetime=True)
        if map_traits is not None:
            cur_name = f"traits_complete-program-lifetime{self.cfg.dest_file_postfix}"
            # serializing and writing json
            json_object = CJsonTraitsWrapper.generate_json_dump(self.dc, map_traits)
            with open(os.path.join(self.cfg.destination, f"{cur_name}.json"), "w", newline='') as outfile:
                outfile.write(json_object)
            if trace is not None and self.cfg.gen_trace:
                with open(os.path.join(self.cfg.destination, f"{cur_name}_decision_trace.csv"), "w", newline='') as outfile:
                    trace.write_to_file(outfile)

        # ============================================================
        # == Phase based traits (PBDP)
        # ============================================================
        self.logger.info(f"Creating trait recommendations per phase (PBDP) ...")
        map_traits, trace = r.generate(map_mem_spaces, phase_based=True)
        if map_traits is not None:
            cur_name = f"traits_per-phase{self.cfg.dest_file_postfix}"
            # serializing and writing json
            json_object = CJsonTraitsWrapper.generate_json_dump(self.dc, map_traits)
            with open(os.path.join(self.cfg.destination, f"{cur_name}.json"), "w", newline='') as outfile:
                outfile.write(json_object)
            if trace is not None and self.cfg.gen_trace:
                with open(os.path.join(self.cfg.destination, f"{cur_name}_decision_trace.csv"), "w", newline='') as outfile:
                    trace.write_to_file(outfile)