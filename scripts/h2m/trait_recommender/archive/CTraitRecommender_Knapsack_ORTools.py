
from .CTraitRecommenderBase import *
from ..common import CLogging
from ..common.CAggregationStats_V1 import CAggregationStats_V1
from ..common.CTraitEntry import *
from typing import Tuple
from ortools.algorithms.python import knapsack_solver
from timeit import default_timer as timer

class CTraitRecommender_Knapsack_ORTools(CTraitRecommenderBase):

    logger = CLogging.get_my_logger("Recommender_Knapsack_ORTools")

    def __init__(self):
        pass

    def get_traits_for_phase(self, map_mem_spaces: dict[ETraitValue, CMemoryKind], phase: CPhase = None) -> dict[CAllocation, list[CTraitEntry]]:
        # gather all relevant data accesses
        all_acc = self.dc.get_all_accesses() if phase is None else self.dc.get_all_accesses_for_phase(phase.id)
        if len(all_acc) == 0:
            return None
        
        # get all aggregations and allocations
        dict_aggr_alloc = self.dc.map_aggregation_allocs if phase is None else self.dc.map_phase_accesses[phase.id]
                
        # map with traits per aggregation entity
        traits: dict[CAllocation, list[CTraitEntry]] = {}

        # calculate stats for each allocation entity
        list_stats: list[Tuple[CAggregation, CAllocation, CAggregationStats_V1, list[CTraitEntry]]] = []
        for aggr, allocs in dict_aggr_alloc.items():
            for al in allocs:
                tmp_stats = self.instances.inst_aggregation_stats.get_stats(self.cfg, [al], all_acc)
                list_stats.append((aggr, al, tmp_stats, []))
        
        # sort based on preference: here required to have chronological order for later postprocessing
        # idea: keep the correct order for same callsite
        stats_sorted = sorted(list_stats, key = lambda x: x[1].date_alloc_ms, reverse=False)

        # prepare data for knapsack
        array_values        = []
        array_weights_bytes = []
        array_alloc_ids     = []
        ms                  = map_mem_spaces[ETraitValue.MEM_SPACE_HBW]
        max_capacity_bytes  = int(ms.capacity_MB * 1_000_000)

        for _, al, stat, cur_traits in stats_sorted:
            # array_values.append(int(stat.exp_gain * 1_000_000))
            array_values.append(int(stat.pct_weight_memory * 1_000_000))
            array_weights_bytes.append(int(al.size_mb * 1_000_000))
            array_alloc_ids.append(al.id)

        # === DEBUG ===
        # print("Values:", array_values)
        # print("Weights:", array_weights_bytes)
        # print("IDs:", array_alloc_ids)
        # print("MaxCap:", max_capacity_bytes)
        # === DEBUG ===

        # run solver
        start = timer()
        solver = knapsack_solver.KnapsackSolver(
            knapsack_solver.SolverType.KNAPSACK_MULTIDIMENSION_BRANCH_AND_BOUND_SOLVER,
            # knapsack_solver.SolverType.KNAPSACK_DYNAMIC_PROGRAMMING_SOLVER,
            # knapsack_solver.SolverType.KNAPSACK_MULTIDIMENSION_CBC_MIP_SOLVER,
            "Knapsack",
        )
        solver.init(array_values, [array_weights_bytes], [max_capacity_bytes])
        computed_value = solver.solve()

        packed_items, packed_weights = [], []
        total_weight = 0
        for i in range(len(array_values)):
            if solver.best_solution_contains(i):
                packed_items.append(array_alloc_ids[i])
                packed_weights.append(array_weights_bytes[i])
                total_weight += array_weights_bytes[i]
        end = timer()

        # === DEBUG ===
        # print("Elapsed time for knapsack:", (end - start))
        # print("Total value:", computed_value)
        # print("Total weight:", total_weight)
        # print("Packed items:", packed_items)
        # print("Packed_weights:", packed_weights)
        # === DEBUG ===

        for aggr, al, stat, cur_traits in stats_sorted:
            desired_mem_space = ETraitValue.MEM_SPACE_LARGE_CAP
            if al.id in packed_items:
                desired_mem_space = ETraitValue.MEM_SPACE_HBW
            cur_traits.append(CTraitEntry("h2m_atk_req_mem_space", ETraitValue.get_str(desired_mem_space), EValueType.ATV))
            traits[al] = cur_traits

        return traits
    
    def generate(self, map_mem_spaces: dict[ETraitValue, CMemoryKind], phase_based: bool = False) -> dict[CPhase, dict[CAllocation, list[CTraitEntry]]]:
        map_traits: dict[CPhase, dict[CAllocation, list[CTraitEntry]]] = {}

        if phase_based:
            for ph in self.dc.phases:
                # reset capacity for every phase
                for ms in map_mem_spaces.values():
                    ms.tmp_cap_limit_MB = ms.capacity_MB

                # get traits
                tmp_traits = self.get_traits_for_phase(map_mem_spaces, ph)
                if tmp_traits is not None:
                    map_traits[ph] = tmp_traits
        else:
            # reset capacity for every phase
            for ms in map_mem_spaces.values():
                ms.tmp_cap_limit_MB = ms.capacity_MB

            ph = CPhase(-1, "Complete Program", -1, -1)
            map_traits[ph] = self.get_traits_for_phase(map_mem_spaces)

        return map_traits