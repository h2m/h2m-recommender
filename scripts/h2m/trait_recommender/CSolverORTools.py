from ortools.algorithms.python import knapsack_solver
from timeit import default_timer as timer

@staticmethod
def solve_knapsack(max_capacity_bytes, array_values, array_weights_bytes, array_alloc_ids, currently_in_fast_mem, cost_slow_to_fast, cost_fast_to_slow):
    # Not yet implemented
    # return obj_val, total_weight, packed_items, packed_weights
    return None