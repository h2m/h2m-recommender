import os
import sys
from .CTraitRecommenderBase import *
from ..common import CLogging
from ..common.CAggregationStats_V1 import CAggregationStats_V1
from ..common.CTraitEntry import *
from ..common.CJsonObjects import CJsonTraitsWrapper
from ..common.CDecisionTrace import CDecisionTrace
from ..common.CStatisticsCache import CStatisticsCache
from typing import Tuple
from enum import Enum

class ObjFunctionVariant(Enum):
    GAIN_AND_TRANSFER_TIME = 1
    GAIN = 2
    AGGR_LATENCY = 3
    GAIN_AND_TRANSFER_TIME_DECAY = 4

class CTraitRecommender_Knapsack(CTraitRecommenderBase):

    logger = CLogging.get_my_logger("Recommender_Knapsack")

    def __init__(self):

        # init caches
        self.stats_caches = CStatisticsCache()
        self.stats_caches.reset_caches()

        # ==============================
        # Read environment variables
        # ==============================
        allowed_solver_types = ["gurobi", "ortools", "lpsolve"]
        self.solver_type = os.getenv('KS_SOLVER_TYPE')
        if self.solver_type is None:
            self.solver_type = "gurobi"
            # self.solver_type = "lpsolve"
        else:
            self.solver_type = self.solver_type.lower()
        if self.solver_type not in allowed_solver_types:
            CTraitRecommender_Knapsack.logger.error(f"Invalid value for env variable KS_SOLVER_TYPE. Needs to be one of the following: {allowed_solver_types}")
            exit(-42)

        self.arr_next_n_phases = os.getenv('KS_ARR_NEXT_N_PHASES')
        if self.arr_next_n_phases is None:
            CTraitRecommender_Knapsack.logger.warning(f"KS_ARR_NEXT_N_PHASES not set.")
            self.arr_next_n_phases = None
        else:
            self.arr_next_n_phases = [int(1e9) if x.strip()=="all" else int(x) for x in self.arr_next_n_phases.split(",")]

        self.sampling_freq = os.getenv('KS_SAMPLING_FREQ')
        if self.sampling_freq is None:
            CTraitRecommender_Knapsack.logger.warning(f"KS_SAMPLING_FREQ not set. Defaulting to 6000.")
            self.sampling_freq = 6000
        else:
            self.sampling_freq = int(self.sampling_freq)

        self.obj_fcn_variant = os.getenv('KS_OBJ_FNC_VARIANT')
        if self.obj_fcn_variant is None:
            CTraitRecommender_Knapsack.logger.warning(f"KS_OBJ_FNC_VARIANT not set. Defaulting to GAIN_AND_TRANSFER_TIME.")
            self.obj_fcn_variant = ObjFunctionVariant.GAIN_AND_TRANSFER_TIME
        else:
            self.obj_fcn_variant = [member for member in ObjFunctionVariant if member.name.lower() == self.obj_fcn_variant.strip().lower()][0]

        self.pm_weighting_factor = os.getenv('OPT_PM_WEIGHTING_FACTOR')
        if self.pm_weighting_factor is None:
            # CTraitRecommender_Knapsack.logger.warning(f"OPT_PM_WEIGHTING_FACTOR not set. Defaulting to 1.")
            self.pm_weighting_factor = 1
        else:
            self.pm_weighting_factor = int(self.pm_weighting_factor)

        CTraitRecommender_Knapsack.logger.info(f"Configuration: solver_type={self.solver_type}, sampling_freq={self.sampling_freq}, obj_fcn_variant={self.obj_fcn_variant.name}")

    def __determine_items_to_move_out(self,
                                      cap_fast_mem_MB: float,
                                      cur_in_fast_mem: list[int],
                                      should_be_in_fast: list[int]) -> list[int]:
        # calculate remaining capacity
        remain_cap = cap_fast_mem_MB - sum([x.size_mb for x in self.dc.allocations if x.id in should_be_in_fast])
        ids_move_out = []
        ids_candidates = list(set(cur_in_fast_mem) - set(should_be_in_fast))
        allocs_move_out = sorted([x for x in self.dc.allocations if x.id in ids_candidates], key = lambda x: x.size_mb, reverse=False)

        for al in allocs_move_out:
            # if al.size_mb <= remain_cap:
            #     # minimize additional data movement as much as possible
            #     remain_cap -= al.size_mb
            # else:
            ids_move_out.append(al.id)
        return ids_move_out

    def __get_traits_for_decisions(self,
                                   cur_in_fast_mem: list[int],
                                   should_be_in_fast: list[int],
                                   should_move_to_slow: list[int],
                                   should_prefetch: list[int]) -> dict[CAllocation, list[CTraitEntry]]:
        # init return value
        traits: dict[CAllocation, list[CTraitEntry]] = {}

        # first handle movement to LARGE_CAP memory to avoid problems
        for al in self.dc.allocations:
            cur_traits: list[CTraitEntry] = []
            if al.id in should_be_in_fast and al.id in should_move_to_slow:
                CTraitRecommender_Knapsack.logger.error("Something wrong here. Allocation {al.id} cannot be in fast and slow at the same time.")
                exit(-3)
            if al.id in should_move_to_slow:
                cur_traits.append(CTraitEntry("h2m_atk_req_mem_space", ETraitValue.get_str(ETraitValue.MEM_SPACE_LARGE_CAP), EValueType.ATV))
                traits[al] = cur_traits
        
        # next handle movement to HBW memory
        for al in self.dc.allocations:
            cur_traits: list[CTraitEntry] = []
            # always output that because in prefetching case it might be that data item not yet there. So decision needs to noted
            # runtime side will filter out unnecessary movements anyway
            if al.id in should_be_in_fast:
                cur_traits.append(CTraitEntry("h2m_atk_req_mem_space", ETraitValue.get_str(ETraitValue.MEM_SPACE_HBW), EValueType.ATV))
                tmp_cpy = CAllocation(al)
                tmp_cpy.daccesses = al.daccesses
                tmp_cpy.prefetch = int(al.id in should_prefetch)
                traits[tmp_cpy] = cur_traits
        
        return traits
    
    def __optimize_placement(self,
                             current_phase: CPhase,
                             ph_idx_start: int,
                             ph_idx_end: int,
                             data_combined: dict[CAggregation, list[CAllocation]],
                             cap_fast_mem_MB: float,
                             cur_in_fast_mem: list[int]):
        # get all accesses for data (required for modeling)
        all_acc = CDataContainer.get_all_accesses_for_custom_data(data_combined)
        if all_acc.ts_ms.size == 0:
            return []

        # calculate stats for each allocation entity
        list_stats: list[Tuple[CAllocation, CAggregationStats_V1]] = []

        apply_gain_decay = self.obj_fcn_variant == ObjFunctionVariant.GAIN_AND_TRANSFER_TIME_DECAY

        for _, allocs in data_combined.items():
            for al in allocs:
                # check whether statistics need to be calculated
                key = (ph_idx_start, al.id)
                if key in self.stats_caches.cache_alloc_stats_combined.keys():
                    tmp_stats = self.stats_caches.cache_alloc_stats_combined[key]
                    self.stats_caches.cache_alloc_stats_combined_hits += 1
                else:
                    tmp_stats = self.instances.inst_aggregation_stats.get_stats(self.cfg, [al], all_acc)
                    self.stats_caches.cache_alloc_stats_combined[key] = tmp_stats
                    self.stats_caches.cache_alloc_stats_combined_misses += 1

                # apply decay depending on strategy and calculated/update expected and gain
                if apply_gain_decay:
                    n_phases = ph_idx_end - ph_idx_start + 1
                    # only apply decay if multiple phases are considered
                    if n_phases > 1:
                        tmp_gain = CAggregationStats_V1.calculate_gain_with_decay(
                            self.instances,
                            self.cfg,
                            self.dc,
                            self.stats_caches,
                            al,
                            ph_idx_start,
                            ph_idx_end,
                            all_acc)

                        # update gain in original stats object
                        tmp_stats.exp_gain = tmp_gain

                # add allocation statistics for optimization strategy
                list_stats.append((al, tmp_stats))
        
        use_new_obj = self.obj_fcn_variant in [ObjFunctionVariant.GAIN_AND_TRANSFER_TIME, ObjFunctionVariant.GAIN_AND_TRANSFER_TIME_DECAY]

        # prepare data for knapsack
        array_values                = []
        array_already_allocated     = []
        array_weights_bytes         = []
        array_alloc_ids             = []
        array_currently_in_fast     = []
        array_copy_costs_to_fast    = []
        array_copy_costs_to_slow    = []
        max_capacity_bytes          = int(cap_fast_mem_MB * 1_000_000)

        for al, stat in list_stats:
            cur_transfer_time_to_fast = stat.exp_transfer_time_to_fast
            cur_transfer_time_to_slow = stat.exp_transfer_time_to_slow
            array_alloc_ids.append(al.id)
            array_weights_bytes.append(int(al.size_mb * 1_000_000))
            array_already_allocated.append(int(al.date_alloc_ms < current_phase.ts_ms_start))
            array_currently_in_fast.append(int(al.id in cur_in_fast_mem))
            array_copy_costs_to_fast.append(int(cur_transfer_time_to_fast * 1_000_000))
            array_copy_costs_to_slow.append(int(cur_transfer_time_to_slow * 1_000_000))

            if self.obj_fcn_variant == ObjFunctionVariant.AGGR_LATENCY:
                # Alternative: higher values indicate that placing it into fast memory with lower latency will be beneficial
                array_values.append(int(stat.pct_weight_memory * 1_000_000))
            else:
                array_values.append(int(stat.exp_gain * self.sampling_freq * 1_000_000))

        # print(f"max_capacity: {max_capacity_bytes}")
        # print(f"array_already_allocated: {array_already_allocated}")
        # print(f"array_currently_in_fast: {array_currently_in_fast}")
        # print(f"array_values: {[x for x in array_values]}")
        # print(f"array_weights: {[x for x in array_weights_bytes]}")
        # print(f"array_copy_costs_to_fast: {[x for x in array_copy_costs_to_fast]}")
        # print(f"array_copy_costs_to_slow: {[x for x in array_copy_costs_to_slow]}")
        # print(f"array_alloc_ids: {array_alloc_ids}")
        # print("\n")

        # Optimization
        if self.solver_type == "gurobi":
            from .CSolverGurobi import CSolverGurobi
            obj_val, total_weight, packed_items, _ = CSolverGurobi.solve_knapsack(
                max_capacity_bytes, array_values, array_weights_bytes, array_alloc_ids, array_already_allocated,
                array_currently_in_fast, array_copy_costs_to_fast, array_copy_costs_to_slow, new_obj=use_new_obj
            )
        elif self.solver_type == "ortools":
            CTraitRecommender_Knapsack.logger.error(f"ortools solver not yet implemented with objective function definition")
            exit(-2)
        elif self.solver_type == "lpsolve":
            from .CSolverLPSolve import CSolverLPSolve
            obj_val, total_weight, packed_items, _ = CSolverLPSolve.solve_knapsack(
                max_capacity_bytes, array_values, array_weights_bytes, array_alloc_ids, array_already_allocated,
                array_currently_in_fast, array_copy_costs_to_fast, array_copy_costs_to_slow, new_obj=use_new_obj
            )

        return packed_items

    def __filter_out_allocations(self,
                                 data: dict[CAggregation, list[CAllocation]],
                                 list_alloc_ids) -> dict[CAggregation, list[CAllocation]]:
        relevant_allocs: dict[int, CAllocation] = {}
        for _, list_alloc in data.items():
            for al in list_alloc:
                if al.id not in list_alloc_ids:
                    relevant_allocs[al.id] = al

        ret: dict[CAggregation, list[CAllocation]] = {}
        unqiue_aggr_id = list(set([x.aggregation_id for x in relevant_allocs.values()]))
        for aggr_id in unqiue_aggr_id:
            cur_aggr = [x for x in self.dc.map_aggregation_allocs.keys() if x.id == aggr_id][0]
            tmp_allocs = [x for x in relevant_allocs.values() if x.aggregation_id == aggr_id]
            ret[cur_aggr] = tmp_allocs

        return ret

    def __get_data_for_phase(self, phase: CPhase) -> dict[CAggregation, list[CAllocation]]:
        if phase.id not in self.dc.map_phase_accesses.keys():
            return None
        return self.dc.map_phase_accesses[phase.id]
    
    def __get_combined_data_for_next_phases(self,
                                            ph_idx_start: int,
                                            ph_idx_end: int,
                                            cur_in_fast_mem: list[int] = []) -> dict[CAggregation, list[CAllocation]]:

        # determine relevant allocations
        relevant_allocs: dict[int, CAllocation] = {}
        
        for idx in range(ph_idx_start, ph_idx_end+1):
            ph_obj = self.dc.phases[idx]

            # make sure there is data for this phase
            if ph_obj.id not in self.dc.map_phase_accesses.keys():
                continue

            # also consider allocations that are currently in fast memory and still overlap with one of the desired phases as relevant
            # some of those might still fit into fast memory, so avoid additional transfers out of fast memory as much as possible
            for al in self.dc.allocations:
                if al.id in cur_in_fast_mem:
                    # check whether allocation is overlapping with phase
                    overlap = not (al.date_dealloc_ms < ph_obj.ts_ms_start or al.date_alloc_ms > ph_obj.ts_ms_end)
                    if overlap and al.id not in relevant_allocs.keys():
                        new_al = CAllocation(al)
                        relevant_allocs[al.id] = new_al

            cur_aggr_allocs = self.dc.map_phase_accesses[ph_obj.id]
            for _, allocs in cur_aggr_allocs.items():
                for al in allocs:
                    if al.id not in relevant_allocs.keys():
                        new_al = CAllocation(al)
                        new_al.daccesses.addTo(al.daccesses)
                        relevant_allocs[al.id] = new_al
                    else:                    
                        cur_al = relevant_allocs[al.id]
                        cur_al.daccesses.addTo(al.daccesses)

        # group by aggregation again
        ret: dict[CAggregation, list[CAllocation]] = {}
        unqiue_aggr_id = list(set([x.aggregation_id for x in relevant_allocs.values()]))
        for aggr_id in unqiue_aggr_id:
            cur_aggr = [x for x in self.dc.map_aggregation_allocs.keys() if x.id == aggr_id][0]
            tmp_allocs = [x for x in relevant_allocs.values() if x.aggregation_id == aggr_id]
            ret[cur_aggr] = tmp_allocs

        return None if len(ret) == 0 else ret

    def __default_trait_gen(self,
                            ph_start_idx: int,
                            ph_end_idx: int,
                            data_combined: dict[CAggregation, list[CAllocation]],
                            cap_fast_mem_MB: float,
                            cur_in_fast_mem: list[int],
                            map_traits: dict[CPhase, dict[CAllocation, list[CTraitEntry]]],
                            prefetch_all: bool = False):

        # get current reference phase (which is the starting phase)
        ph_start = self.dc.phases[ph_start_idx]

        # optimize data placement
        packed_ids    = self.__optimize_placement(ph_start, ph_start_idx, ph_end_idx, data_combined, cap_fast_mem_MB, cur_in_fast_mem)
        ids_move_out  = self.__determine_items_to_move_out(cap_fast_mem_MB, cur_in_fast_mem, packed_ids)
        tmp_traits    = self.__get_traits_for_decisions(cur_in_fast_mem, packed_ids, ids_move_out, packed_ids if prefetch_all else [])

        if tmp_traits is not None:
            map_traits[ph_start] = tmp_traits
            cur_in_fast_mem = list(set(cur_in_fast_mem) - set(ids_move_out))
            cur_in_fast_mem = list(set(cur_in_fast_mem).union(set(packed_ids)))
        return cur_in_fast_mem, ids_move_out
    
    def __generate_for_next_n_phases(self,
                                     cap_fast_mem_MB: float,
                                     n_next_phases: int = 1) -> Tuple[dict[CPhase, dict[CAllocation, list[CTraitEntry]]], CDecisionTrace]:
        map_traits = {}
        cur_in_fast_mem = []
        ids_move_out = []
        num_phases = len(self.dc.phases)

        # initialize decision trace here
        decision_trace = CDecisionTrace(self.dc,
                                        self.cfg,
                                        self.instances,
                                        sampling_freq=self.sampling_freq,
                                        stats_caches=self.stats_caches,
                                        apply_gain_decay=self.obj_fcn_variant == ObjFunctionVariant.GAIN_AND_TRANSFER_TIME_DECAY)

        for ph_idx_start in range(num_phases):

            # determine last phase index to consider
            ph_idx_end = ph_idx_start + n_next_phases - 1   # inclusive
            # safety check to avoid out of bounds
            if ph_idx_end >= len(self.dc.phases):
                ph_idx_end = len(self.dc.phases) - 1

            # get current phase obj
            ph_start = self.dc.phases[ph_idx_start]

            # print(f"--- Generating for Phase Index {ph_idx_start}")

            # get relevant data for phase optimization
            data_combined = self.__get_combined_data_for_next_phases(ph_idx_start, ph_idx_end, cur_in_fast_mem=cur_in_fast_mem)

            if data_combined is not None:
                # run optimization and trait generation
                cur_in_fast_mem, ids_move_out = self.__default_trait_gen(ph_idx_start, ph_idx_end, data_combined, cap_fast_mem_MB, cur_in_fast_mem, map_traits)

                if self.cfg.gen_trace:
                    # update decision trace for phase index
                    decision_trace.update_decision_trace(ph_idx_start, ph_idx_end, data_combined, cur_in_fast_mem, ids_move_out)
        
        return map_traits, decision_trace

    # def __generate_lookahead(self, cap_fast_mem_MB: float) -> dict[CPhase, dict[CAllocation, list[CTraitEntry]]]:
    #     map_traits = {}
    #     cur_in_fast_mem = []

    #     for ph_idx in range(len(self.dc.phases)):
    #         # get current phase obj
    #         ph = self.dc.phases[ph_idx]
            
    #         # check if there is a next phase
    #         ph_idx_after = ph_idx + 1
    #         ph_after = None if ph_idx_after >= len(self.dc.phases) else self.dc.phases[ph_idx_after]

    #         print(f"--- Generating for Phase Index {ph_idx}")

    #         if ph_after is None:
    #             # ====================================
    #             # Case: there is only one phase left
    #             #   ==> Default blocking transfer
    #             # ====================================
    #             # get relevant data for phase optimization
    #             data = self.__get_data_for_phase(ph)
    #             if data is not None:
    #                 cur_in_fast_mem, _ = self.__default_trait_gen(ph, data, cap_fast_mem_MB, cur_in_fast_mem, map_traits)
    #         else:
    #             # ====================================
    #             # Case: there is a second phase
    #             # ====================================
    #             # get relevant data for phase optimization
    #             data_phase      = self.__get_data_for_phase(ph)
    #             data_combined   = self.__get_combined_data_for_next_phases(ph_idx, ph_idx_after+1)

    #             # skip case if no data available
    #             if data_phase is None and data_combined is None:
    #                 continue

    #             if data_phase is None:
    #                 # combined contains only second phase and can be fully prefetched
    #                 cur_in_fast_mem, _ = self.__default_trait_gen(ph, data_combined, cap_fast_mem_MB, cur_in_fast_mem, map_traits, prefetch_all=True)
    #             else:
    #                 # optimize placement
    #                 packed_ids_p1   = self.__optimize_placement(ph, data_phase,    cap_fast_mem_MB, cur_in_fast_mem)
    #                 packed_ids_comb = self.__optimize_placement(ph, data_combined, cap_fast_mem_MB, cur_in_fast_mem)

    #                 # items that appear in both should definitely go to fast memory
    #                 should_be_in_fast = list(set(packed_ids_p1).intersection(set(packed_ids_comb)))

    #                 # handle remaining data
    #                 data_remain = self.__filter_out_allocations(data_combined, should_be_in_fast)
    #                 cap_fast_remain = cap_fast_mem_MB - sum([x.size_mb for x in self.dc.allocations if x.id in should_be_in_fast])                
    #                 packed_ids_remain = self.__optimize_placement(ph, data_remain, cap_fast_remain, cur_in_fast_mem)
    #                 should_be_in_fast = list(set(should_be_in_fast).union(packed_ids_remain))

    #                 # determine allocations that should move out of fast mem
    #                 should_move_to_slow = self.__determine_items_to_move_out(cap_fast_mem_MB, cur_in_fast_mem, should_be_in_fast)
    #                 # determine ids for prefetching
    #                 should_prefetch = [x for x in should_be_in_fast if x not in cur_in_fast_mem and x not in packed_ids_p1]

    #                 # finally generate traits
    #                 tmp_traits = self.__get_traits_for_decisions(cur_in_fast_mem, should_be_in_fast, should_move_to_slow, should_prefetch)
    #                 map_traits[ph] = tmp_traits
                    
    #                 # assume that transfers wait or are finished in the next transition
    #                 cur_in_fast_mem = list(set(cur_in_fast_mem) - set(should_move_to_slow))
    #                 cur_in_fast_mem = list(set(cur_in_fast_mem).union(set(should_be_in_fast)))
        
    #     return map_traits

    def find_allocs_that_overlap(self, cur_al: CAllocation, list_allocs: list[CAllocation]) -> list[CAllocation]:
        ret_allocs = []
        for al in list_allocs:
            disjoint = (al.date_dealloc_ms < cur_al.date_alloc_ms)  or (cur_al.date_dealloc_ms < al.date_alloc_ms)
            overlap = not disjoint
            # overlap = (al.date_alloc_ms >= cur_al.date_alloc_ms and al.date_alloc_ms <= cur_al.date_dealloc_ms) or (al.date_dealloc_ms >= cur_al.date_alloc_ms and al.date_dealloc_ms <= cur_al.date_dealloc_ms)
            if overlap:
                ret_allocs.append(al)
        return ret_allocs

    def generate_complete_idp(self,
                          ph: CPhase,
                          cap_fast_mem_MB: float,
                          map_traits: dict[CPhase, dict[CAllocation, list[CTraitEntry]]],
                          respect_lifetime: bool = False):

        allocs_in_fast  = []
        allocs_in_slow  = []

        if not respect_lifetime:
            allocs_in_fast = self.__optimize_placement(ph, 0, 0, self.dc.map_aggregation_allocs, cap_fast_mem_MB, [])
            allocs_in_slow = list(set([x.id for x in self.dc.allocations]) - set(allocs_in_fast))
        else:
            # queues that hold allocations or allocation ids
            sorted_allocs   = sorted(self.dc.allocations, key=lambda x: (x.date_dealloc_ms - x.date_alloc_ms), reverse=True)
            sorted_allocs   = [x for x in sorted_allocs if x.size_mb > 2] # filter out very small data items
            remain_allocs   = list(sorted_allocs)

            while len(remain_allocs) != 0:
                # print(f"Allocs left: {len(remain_allocs)}")
                min_size = min([x.size_mb for x in remain_allocs])
                # look at current allocation
                cur_alloc = remain_allocs.pop(0)

                # get allocations that overlap with it
                allocs_overlap = self.find_allocs_that_overlap(cur_alloc, sorted_allocs)
                allocs_final = [cur_alloc]

                # determine capacity
                cur_limit = cap_fast_mem_MB
                for al in allocs_overlap:
                    if al.id in allocs_in_fast:
                        cur_limit -= al.size_mb
                    elif al.id not in allocs_in_slow:
                        allocs_final.append(al)

                if cur_limit < max([min_size, 2]):
                    allocs_in_slow.append(cur_alloc.id)
                    continue

                # map by aggregation
                unique_aggr = list(set([x.aggregation_id for x in allocs_final]))
                tmp_data = {}
                for aggr_id in unique_aggr:
                    aggr = [x for x in self.dc.map_aggregation_allocs.keys() if x.id == aggr_id][0]
                    tmp_list = [x for x in allocs_final if x.aggregation_id == aggr_id]
                    tmp_data[aggr] = tmp_list

                # optimize placement for overlapping items
                packed_ids = self.__optimize_placement(ph, 0, 0, tmp_data, cur_limit, [])

                # evaluate and assign to memory spaces
                allocs_in_fast.extend(packed_ids)
                for pid in packed_ids:
                    if pid != cur_alloc.id:
                        tmp = [x for x in remain_allocs if x.id == pid][0]
                        remain_allocs.remove(tmp)
                if cur_alloc.id not in packed_ids:
                    # this allocation definitly needs to go to slow mem
                    allocs_in_slow.append(cur_alloc.id)

        # finally generate traits for placement decisions
        tmp_traits = self.__get_traits_for_decisions([], allocs_in_fast, allocs_in_slow, [])

        # and create decision trace
        decision_trace = None
        if tmp_traits is not None:
            map_traits[ph] = tmp_traits

            if self.cfg.gen_trace:
                # initialize decision trace here
                decision_trace = CDecisionTrace(self.dc,
                                                self.cfg,
                                                self.instances,
                                                sampling_freq=self.sampling_freq,
                                                phase_based=False,
                                                stats_caches=self.stats_caches)

                # update decision trace for phase index
                decision_trace.update_decision_trace(0, 0, self.dc.map_aggregation_allocs, allocs_in_fast, [])

        return decision_trace

    def generate(self, map_mem_spaces: dict[ETraitValue, CMemoryKind], phase_based: bool = False, respect_lifetime: bool = False) -> dict[CPhase, dict[CAllocation, list[CTraitEntry]]]:
        cap_fast_mem_MB = map_mem_spaces[ETraitValue.MEM_SPACE_HBW].capacity_MB
        self.logger.info(f"Capacity limit for HBW set to {cap_fast_mem_MB} MB")
        self.stats_caches.reset_caches()

        if phase_based:
            # ============================================================
            # == For the next upcoming N phases (blocking migration)
            # ============================================================
            if not self.arr_next_n_phases is None:
                for n in self.arr_next_n_phases:
                    self.stats_caches.reset_caches()
                    self.logger.info(f"Generate per-phase - Considering data of the upcoming {n} phases")
                    map_traits, trace = self.__generate_for_next_n_phases(cap_fast_mem_MB, n_next_phases=n)
                    json_object = CJsonTraitsWrapper.generate_json_dump(self.dc, map_traits)
                    str_upcoming = f"{n}" if n != 1e9 else "all"
                    cur_name = f"traits_per-phase{self.cfg.dest_file_postfix}_upcoming_{str_upcoming}"
                    # output traits
                    with open(os.path.join(self.cfg.destination, f"{cur_name}.json"), "w", newline='') as outfile:
                        outfile.write(json_object)

                    if trace is not None and self.cfg.gen_trace:
                        # output decision trace
                        with open(os.path.join(self.cfg.destination, f"{cur_name}_decision_trace.csv"), "w", newline='') as outfile:
                            trace.write_to_file(outfile)
                    self.stats_caches.print_cache_ctrs()

            # ============================================================
            # == Lookahead version (prefetch + async) --> quite tricky
            # ============================================================
            # self.logger.info("Generate per-phase - blocking + prefetching")
            # map_traits = self.__generate_lookahead(cap_fast_mem_MB)
            # json_object = CJsonTraitsWrapper.generate_json_dump(self.dc, map_traits)
            # with open(os.path.join(self.cfg.destination, f"traits_per-phase{self.cfg.dest_file_postfix}_lookahead.json"), "w", newline='') as outfile:
            #     outfile.write(json_object)
            
            # return None to avoid file generation outside
            return None, None
        else:
            self.stats_caches.reset_caches()
            if self.pm_weighting_factor == 1:
                map_traits: dict[CPhase, dict[CAllocation, list[CTraitEntry]]] = {}
                ph = CPhase(-1, "Complete Program", -1, 9223372036854775807)
                trace = self.generate_complete_idp(ph, cap_fast_mem_MB, map_traits, respect_lifetime=respect_lifetime)
            else:
                # avoid generating traits for IDP if pm_weighting_factor is not 1, because it does not make a difference
                map_traits, trace = None, None
            self.stats_caches.print_cache_ctrs()

        return map_traits, trace