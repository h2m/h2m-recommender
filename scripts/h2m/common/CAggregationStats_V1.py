from .CAggregationStatsBase import CAggregationStatsBase
from .CAllocationStats_V1 import CAllocationStats_V1
from .CDataTypes import *
from .CStatisticsCache import CStatisticsCache
from .CManualInstances import CManualInstances

import statistics as st
import os

class CAggregationStats_V1(CAggregationStatsBase):
    
    def __init__(self) -> None:
        # allocation related
        self.nr_allocs              = 0
        self.total_size_mb          = 0
        self.avg_size_mb            = 0

        # weight / latency related
        self.weight_total           = 0
        self.weight_memory          = 0
        self.weight_avg             = 0
        self.weight_phase_total     = 0
        self.weight_phase_memory    = 0

        # access related
        self.acc_total              = 0
        self.acc_read               = 0
        self.acc_read_cache         = 0
        self.acc_read_memory        = 0
        self.acc_write              = 0
        self.acc_write_cache        = 0
        self.acc_write_memory       = 0
        self.acc_cache              = 0
        self.acc_memory             = 0
        self.acc_total_phase        = 0
        self.acc_read_phase         = 0
        self.acc_write_phase        = 0

        # percentages
        self.pct_read               = 0
        self.pct_read_cache         = 0
        self.pct_read_memory        = 0
        self.pct_write              = 0
        self.pct_write_cache        = 0
        self.pct_write_memory       = 0
        self.pct_acc_cache          = 0
        self.pct_acc_memory         = 0
        self.pct_acc_in_phase       = 0
        self.pct_reads_in_phase     = 0
        self.pct_write_in_phase     = 0
        self.pct_weight_total       = 0
        self.pct_weight_memory      = 0

        # special percentages that calc importance of current phase based on weight
        self.pct_weight_self_total  = 0
        self.pct_weight_self_memory = 0

        # performance modeling (all times are in seconds)
        self.exp_acc_time_read_fast_part_bw = 0
        self.exp_acc_time_read_fast_part_lat = 0
        self.exp_acc_time_read_slow_part_bw = 0
        self.exp_acc_time_read_slow_part_lat = 0
        self.exp_acc_time_write_fast_part_bw = 0
        self.exp_acc_time_write_fast_part_lat = 0
        self.exp_acc_time_write_slow_part_bw = 0
        self.exp_acc_time_write_slow_part_lat = 0
        self.exp_acc_time_read_fast = 0
        self.exp_acc_time_read_slow = 0
        self.exp_acc_time_write_fast = 0
        self.exp_acc_time_write_slow = 0
        self.exp_acc_time_fast = 0
        self.exp_acc_time_slow = 0
        self.exp_gain = 0
        self.exp_transfer_time_to_fast = 0
        self.exp_transfer_time_to_slow = 0
        
        self.pm_weighting_factor = os.getenv('OPT_PM_WEIGHTING_FACTOR')
        if self.pm_weighting_factor is None:
            # CTraitRecommender_Knapsack.logger.warning(f"OPT_PM_WEIGHTING_FACTOR not set. Defaulting to 1.")
            self.pm_weighting_factor = 1
        else:
            self.pm_weighting_factor = float(self.pm_weighting_factor)

    def get_names(self) -> list[str]:
        names = [
            'nr_allocs',
            'total_size_mb',
            'avg_size_mb',

            'acc_total',
            'acc_read',
            'acc_read_cache',
            'acc_read_memory',
            'acc_write',
            'acc_write_cache',
            'acc_write_memory',
            'acc_cache',
            'acc_memory',
            'acc_total_phase',
            'acc_read_phase',
            'acc_write_phase',

            'weight_total',
            'weight_memory',
            'weight_avg',
            'weight_phase_total',
            'weight_phase_memory',

            'pct_read',
            'pct_read_cache',
            'pct_read_memory',
            'pct_write',
            'pct_write_cache',
            'pct_write_memory',
            'pct_acc_cache',
            'pct_acc_memory',
            'pct_acc_in_phase',
            'pct_reads_in_phase',
            'pct_write_in_phase',
            'pct_weight_total',
            'pct_weight_memory',

            'pct_weight_self_total',
            'pct_weight_self_memory',

            'exp_acc_time_read_fast_part_bw',
            'exp_acc_time_read_fast_part_lat',
            'exp_acc_time_read_slow_part_bw',
            'exp_acc_time_read_slow_part_lat',
            'exp_acc_time_write_fast_part_bw',
            'exp_acc_time_write_fast_part_lat',
            'exp_acc_time_write_slow_part_bw',
            'exp_acc_time_write_slow_part_lat',
            'exp_acc_time_read_fast',
            'exp_acc_time_read_slow',
            'exp_acc_time_write_fast',
            'exp_acc_time_write_slow',
            'exp_acc_time_fast',
            'exp_acc_time_slow',
            'exp_gain',
            'exp_transfer_time_to_fast',
            'exp_transfer_time_to_slow'
        ]
        return names

    def get_base_info_names(self) -> list[str]:
        names = [
            'nr_allocs',
            'total_size_mb',
            'avg_size_mb',
        ]
        return names
    
    def get_stats(self, cfg: CConfig, allocs: list[CAllocation], acc_phase: CDataAccesses, allocs_cs_complete: list[CAllocation] = None) -> 'CAggregationStats_V1':

        # cast allocation stats instance into expected
        al_stats: CAllocationStats_V1
        al_stats = self.alloc_stats
        stats = CAggregationStats_V1()

        # allocation related
        stats.nr_allocs             = len(allocs)
        stats.total_size_mb         = sum([x.size_mb for x in allocs])
        stats.avg_size_mb           = st.mean([x.size_mb for x in allocs]) if stats.nr_allocs > 0 else 0

        # access related
        stats.acc_total             = sum([len(x.daccesses.ts_ms) for x in allocs])
        stats.acc_read              = sum([(x.daccesses.is_write == 0).sum() for x in allocs])
        stats.acc_read_cache        = sum([((x.daccesses.is_write == 0) & (np.isin(x.daccesses.mem_level, CMemoryLevels.MEM_LEVELS_CACHE_READ))).sum() for x in allocs])
        stats.acc_read_memory       = stats.acc_read - stats.acc_read_cache
        stats.acc_write             = stats.acc_total - stats.acc_read
        stats.acc_write_cache       = sum([((x.daccesses.is_write == 1) & (np.isin(x.daccesses.mem_level, CMemoryLevels.MEM_LEVELS_CACHE_WRITE))).sum() for x in allocs])
        stats.acc_write_memory      = stats.acc_write - stats.acc_write_cache
        stats.acc_cache             = stats.acc_read_cache + stats.acc_write_cache
        stats.acc_memory            = stats.acc_read_memory + stats.acc_write_memory
        stats.acc_total_phase       = acc_phase.ts_ms.size if acc_phase is not None else 0
        stats.acc_read_phase        = (acc_phase.is_write == 0).sum() if acc_phase is not None else 0
        stats.acc_write_phase       = stats.acc_total_phase - stats.acc_read_phase

        # weight / latency related
        dummy_alloc_phase           = CAllocation()
        dummy_alloc_phase.daccesses = acc_phase if acc_phase is not None else CDataAccesses()
        phase_stats                 = al_stats.get_stats(dummy_alloc_phase)
        stats.weight_total           = sum([al_stats.get_stats(x).weight_total for x in allocs])
        stats.weight_memory          = sum([al_stats.get_stats(x).weight_memory for x in allocs])
        stats.weight_avg             = 0 if stats.acc_read == 0 else stats.weight_total / stats.acc_read
        if cfg.approximate_writes_to_memory:
            stats.weight_avg         = 0 if (stats.acc_read+stats.acc_write_memory) == 0 else stats.weight_total / (stats.acc_read+stats.acc_write_memory)
        stats.weight_phase_total     = phase_stats.weight_total
        stats.weight_phase_memory    = phase_stats.weight_memory

        # percentages
        stats.pct_read              = stats.acc_read / stats.acc_total if stats.acc_total > 0 else 0
        stats.pct_read_cache        = stats.acc_read_cache / stats.acc_read if stats.acc_read > 0 else 0
        stats.pct_read_memory       = stats.acc_read_memory / stats.acc_read if stats.acc_read > 0 else 0
        stats.pct_write             = stats.acc_write / stats.acc_total if stats.acc_total > 0 else 0
        stats.pct_write_cache       = stats.acc_write_cache / stats.acc_write if stats.acc_write > 0 else 0
        stats.pct_write_memory      = stats.acc_write_memory / stats.acc_write if stats.acc_write > 0 else 0
        stats.pct_acc_cache         = (stats.acc_read_cache+stats.acc_write_cache) / stats.acc_total if stats.acc_total > 0 else 0
        stats.pct_acc_memory        = (stats.acc_read_memory+stats.acc_write_memory) / stats.acc_total if stats.acc_total > 0 else 0
        stats.pct_acc_in_phase      = stats.acc_total / stats.acc_total_phase if stats.acc_total_phase > 0 else 0
        stats.pct_reads_in_phase    = stats.acc_read / stats.acc_read_phase if stats.acc_read_phase > 0 else 0
        stats.pct_write_in_phase    = stats.acc_write / stats.acc_write_phase if stats.acc_write_phase > 0 else 0
        stats.pct_weight_total      = stats.weight_total / stats.weight_phase_total if stats.weight_phase_total > 0 else 0
        stats.pct_weight_memory     = stats.weight_memory / stats.weight_phase_total if stats.weight_phase_total > 0 else 0

        if allocs_cs_complete is not None:
            tmp_weight_total  = sum([al_stats.get_stats(x).weight_total for x in allocs_cs_complete])
            tmp_weight_memory = sum([al_stats.get_stats(x).weight_memory for x in allocs_cs_complete])
            stats.pct_weight_self_total  = float(stats.weight_total) / tmp_weight_total if tmp_weight_total > 0 else 0
            stats.pct_weight_self_memory = float(stats.weight_memory) / tmp_weight_memory if tmp_weight_memory > 0 else 0

        # ==============================
        # performance modeling
        # ==============================
        # scaling factors based on cache hit ratio    
        scaling_factor_bw_read   = stats.pct_read_cache
        scaling_factor_lat_read  = (1 - scaling_factor_bw_read)
        scaling_factor_bw_write  = stats.pct_read_cache # currently use also read cache hit ratio here because SPR system does not get that info for STORE operations.
        scaling_factor_lat_write = (1 - scaling_factor_bw_write)
        
        # option to weight individual terms for bw and lat
        weight_costs_bw = self.pm_weighting_factor
        weight_costs_lat = self.pm_weighting_factor

        # Note: considering all accesses to cache and memory
        tmp_acc_read = stats.acc_read
        tmp_acc_write = stats.acc_write
        # Note: only considering accesses to memory (Problem: somtimes phases only record accesses to cache that will lead to 0 gain and not place object in fast memory)
        # tmp_acc_read = stats.acc_read_memory
        # tmp_acc_write = stats.acc_write_memory

        # Apply bandwidth costs for every access and scale latency costs 
        stats.exp_acc_time_read_fast_part_bw   = (tmp_acc_read * 64 / self.model_bw_read_fast / 1e6) * weight_costs_bw
        stats.exp_acc_time_read_fast_part_lat  = (tmp_acc_read * self.model_lat_read_fast / 1e9 / 8.0 * scaling_factor_lat_read) * weight_costs_lat
        stats.exp_acc_time_read_slow_part_bw   = (tmp_acc_read * 64 / self.model_bw_read_slow / 1e6) * weight_costs_bw
        stats.exp_acc_time_read_slow_part_lat  = (tmp_acc_read * self.model_lat_read_slow / 1e9 / 8.0 * scaling_factor_lat_read) * weight_costs_lat
        stats.exp_acc_time_write_fast_part_bw  = (tmp_acc_write * 64 / self.model_bw_write_fast / 1e6) * weight_costs_bw
        stats.exp_acc_time_write_fast_part_lat = (tmp_acc_write * self.model_lat_write_fast / 1e9 / 8.0 * scaling_factor_lat_write) * weight_costs_lat
        stats.exp_acc_time_write_slow_part_bw  = (tmp_acc_write * 64 / self.model_bw_write_slow / 1e6) * weight_costs_bw
        stats.exp_acc_time_write_slow_part_lat = (tmp_acc_write * self.model_lat_write_slow / 1e9 / 8.0 * scaling_factor_lat_write) * weight_costs_lat

        stats.exp_acc_time_read_fast    = stats.exp_acc_time_read_fast_part_bw + stats.exp_acc_time_read_fast_part_lat
        stats.exp_acc_time_read_slow    = stats.exp_acc_time_read_slow_part_bw + stats.exp_acc_time_read_slow_part_lat
        stats.exp_acc_time_write_fast   = stats.exp_acc_time_write_fast_part_bw + stats.exp_acc_time_write_fast_part_lat
        stats.exp_acc_time_write_slow   = stats.exp_acc_time_write_slow_part_bw + stats.exp_acc_time_write_slow_part_lat

        stats.exp_acc_time_fast         = stats.exp_acc_time_read_fast + stats.exp_acc_time_write_fast
        stats.exp_acc_time_slow         = stats.exp_acc_time_read_slow + stats.exp_acc_time_write_slow
        stats.exp_gain                  = stats.exp_acc_time_slow - stats.exp_acc_time_fast
        stats.exp_transfer_time_to_fast = stats.total_size_mb / self.model_bw_copy_to_fast.get_bw_for_byte_size(stats.total_size_mb * 1_000_000)
        stats.exp_transfer_time_to_slow = stats.total_size_mb / self.model_bw_copy_to_slow.get_bw_for_byte_size(stats.total_size_mb * 1_000_000)

        return stats

    def calculate_gain_with_decay(instances: CManualInstances,
                                  cfg: CConfig,
                                  dc: CDataContainer,
                                  stats_caches: CStatisticsCache,
                                  al: CAllocation,
                                  ph_idx_start: int,
                                  ph_idx_end: int,
                                  all_acc: CDataAccesses):
        tmp_gain = 0.0
        n_phases = ph_idx_end - ph_idx_start + 1
        if n_phases > 1:
            step_size = 1.0 / float((n_phases-1)) * 2.0
            tmp_gain = 0.0

            for i in range(n_phases):
                factor = 2.0 - (i * step_size)

                # get stats for data item and phase
                effective_phase_idx = ph_idx_start + i
                if effective_phase_idx in dc.map_phase_accesses.keys():
                    # find allocation object
                    tmp_map_aggr = dc.map_phase_accesses[effective_phase_idx]
                    for _, allocs2 in tmp_map_aggr.items():
                        match_al = [x for x in allocs2 if x.id == al.id]

                        if len(match_al) > 0:
                            match_al = match_al[0]
                            key2 = (effective_phase_idx, match_al.id)

                            if key2 in stats_caches.cache_alloc_stats_per_phase.keys():
                                stats_al_ph = stats_caches.cache_alloc_stats_per_phase[key2]
                                stats_caches.cache_alloc_stats_per_phase_hits += 1
                            else:
                                stats_al_ph = instances.inst_aggregation_stats.get_stats(cfg, [match_al], all_acc)
                                stats_caches.cache_alloc_stats_per_phase[key2] = stats_al_ph
                                stats_caches.cache_alloc_stats_per_phase_misses += 1
                            tmp_gain += (stats_al_ph.exp_gain * factor)
                            break

        return tmp_gain