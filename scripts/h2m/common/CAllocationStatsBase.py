from abc import ABCMeta, abstractmethod
from .CDataTypes import CConfig, CAllocation, CMemoryInfo

class CAllocationStatsBase(metaclass=ABCMeta):

    cfg: CConfig

    def set_data(self, cfg: CConfig, mem_info: CMemoryInfo):
        self.cfg = cfg
        self.mem_info = mem_info

    @abstractmethod
    def get_stats(self, alloc: CAllocation) -> 'CAllocationStatsBase':
        pass