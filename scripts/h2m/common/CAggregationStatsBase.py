from abc import ABCMeta, abstractmethod
from .CDataTypes import CConfig, CMemoryInfo, CDataAccess, CAllocation
from .CAllocationStatsBase import CAllocationStatsBase

class CAggregationStatsBase(metaclass=ABCMeta):

    cfg: CConfig

    def set_data(self, cfg: CConfig, mem_info: CMemoryInfo, alloc_stats: CAllocationStatsBase):
        self.cfg            = cfg
        self.mem_info       = mem_info
        self.alloc_stats    = alloc_stats   # instance that provides allocation stats

        # temporary for modelling. should be improved when redesigning mem kind ordering
        self.mem_kind_fast  = [x for x in self.mem_info.memory_kinds if x.is_mem_space_hbw][0]
        self.mem_kind_slow  = [x for x in self.mem_info.memory_kinds if x.is_mem_space_large_cap][0]

        self.model_bw_read_fast     = self.mem_kind_fast.scaling_bandwidth_read_only_MBs[cfg.nr_threads-1]
        self.model_bw_read_slow     = self.mem_kind_slow.scaling_bandwidth_read_only_MBs[cfg.nr_threads-1]
        self.model_bw_write_fast    = self.mem_kind_fast.scaling_bandwidth_read_write_MBs[cfg.nr_threads-1]
        self.model_bw_write_slow    = self.mem_kind_slow.scaling_bandwidth_read_write_MBs[cfg.nr_threads-1]

        # for now use idle latency as a reference in nanoseconds
        self.model_lat_read_fast    = self.mem_kind_fast.scaling_latency_read_only_ns[0]
        self.model_lat_read_slow    = self.mem_kind_slow.scaling_latency_read_only_ns[0]
        self.model_lat_write_fast   = self.mem_kind_fast.scaling_latency_read_write_ns[0]
        self.model_lat_write_slow   = self.mem_kind_slow.scaling_latency_read_write_ns[0]

        self.model_bw_copy_to_fast  = [x for x in self.mem_info.copy_bw if x.idx_from == self.mem_kind_slow.idx and x.idx_to == self.mem_kind_fast.idx][0]
        self.model_bw_copy_to_slow  = [x for x in self.mem_info.copy_bw if x.idx_from == self.mem_kind_fast.idx and x.idx_to == self.mem_kind_slow.idx][0]

    @abstractmethod
    def get_names(self) -> list[str]:
        pass

    @abstractmethod
    def get_base_info_names(self) -> list[str]:
        pass

    @abstractmethod
    def get_stats(self, cfg: CConfig, allocs: list[CAllocation], acc_phase: list[CDataAccess], allocs_cs_complete: list[CAllocation] = None) -> 'CAggregationStatsBase':
        pass