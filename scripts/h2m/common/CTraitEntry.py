import enum

class ETraitValue(enum.Enum):
    MEM_SPACE_HBW = 1
    MEM_SPACE_LOW_LAT = 2
    MEM_SPACE_LARGE_CAP = 3

    def get_str(val):
        if val == ETraitValue.MEM_SPACE_HBW:
            return "h2m_atv_mem_space_hbw"
        elif val == ETraitValue.MEM_SPACE_LOW_LAT:
            return "h2m_atv_mem_space_low_lat"
        elif val == ETraitValue.MEM_SPACE_LARGE_CAP:
            return "h2m_atv_mem_space_large_cap"
        return ""

class EValueType(enum.Enum):
    INT = 1
    UINT = 2
    DBL = 3
    LONG = 4
    ATV = 5
    PTR = 6

class CTraitEntry():
    def __init__(self, key: str, value, value_type: EValueType, comment: str = None) -> None:
        self.key = key
        self.value = value
        self.value_type = value_type
        self.comment = comment
    
    def build_line(self) -> str:
        ret = f"{self.key}, "
        if self.value_type == EValueType.INT:
            ret += f"{self.value}"
        elif self.value_type == EValueType.UINT:
            ret += f"{self.value}"
        elif self.value_type == EValueType.DBL:
            ret += f"{{ .dbl = {self.value}}}"
        elif self.value_type == EValueType.LONG:
            ret += f"{{ .l = {self.value}}}"
        elif self.value_type == EValueType.ATV:
            ret += f"{self.value}"
        elif self.value_type == EValueType.PTR:
            ret += f"{{ .ptr = {self.value}}}"

        ret += ", "
        if self.comment:
            ret += f"// {self.comment}"

        return ret

    def reprJSON(self):
        return dict(key=self.key, type=self.value_type._name_ ,value=str(self.value))