from .CAllocationStatsBase import CAllocationStatsBase
from .CDataTypes import *

class CAllocationStats_V1(CAllocationStatsBase):
    
    def __init__(self) -> None:
        # NumaMMA specific
        self.weight_total       = 0
        self.weight_memory      = 0
        self.weight_avg         = 0    
    
    def get_stats(self, alloc: CAllocation) -> 'CAllocationStats_V1':
        if alloc.stats is not None:
            return alloc.stats
        
        stats               = CAllocationStats_V1()
        stats.weight_total  = alloc.daccesses.weight.sum()
        acc_mem             = alloc.daccesses.extract(~np.isin(alloc.daccesses.mem_level, CMemoryLevels.MEM_LEVELS_CACHE_READ))
        acc_mem_read        = acc_mem.extract(acc_mem.is_write == 0)
        # Note: Only consider read accesses for weight as write does not provide full information with NumaMMA
        stats.weight_memory = acc_mem_read.weight.sum()
        num_reads_mem       = acc_mem_read.weight.size
        stats.weight_avg    = 0 if num_reads_mem == 0 else stats.weight_total / num_reads_mem
        
        if self.cfg.approximate_writes_to_memory:
            # Note: additionaly include approximation for writes to memory using avg latency for reads to memory
            num_writes_mem          = acc_mem.ts_ms.size - num_reads_mem
            tmp_weight_writes_mem   = num_writes_mem * self.cfg.avg_weight_read_mem
            stats.weight_total      += tmp_weight_writes_mem
            stats.weight_memory     += tmp_weight_writes_mem
            stats.weight_avg        = 0 if (num_reads_mem+num_writes_mem) == 0 else stats.weight_total / (num_reads_mem+num_writes_mem)
        
        alloc.stats = stats
        return stats