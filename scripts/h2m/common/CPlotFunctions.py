import os
import numpy as np
import matplotlib
matplotlib.use('TKAgg')
import matplotlib.pyplot as plt
from matplotlib import cm
import seaborn as sns; sns.set_theme()

F_SIZE = 16
plt.rc('font', size=F_SIZE)             # controls default text sizes
plt.rc('axes', titlesize=F_SIZE)        # fontsize of the axes title
plt.rc('axes', labelsize=F_SIZE)        # fontsize of the x and y labels
plt.rc('xtick', labelsize=F_SIZE)       # fontsize of the tick labels
plt.rc('ytick', labelsize=F_SIZE)       # fontsize of the tick labels
plt.rc('legend', fontsize=F_SIZE)       # legend fontsize
plt.rc('figure', titlesize=F_SIZE)      # fontsize of the figure title

class CPlotFunctions:

    @staticmethod
    def plot_all(map_callsites, args):
        cur_max_addr            = 0
        list_addr_normalized    = []
        list_ts                 = []
        list_thr                = []
        list_phase_transitions  = []

        for cs, list_files in map_callsites.items():
            cur_max = -1
            for file in list_files:
                l_accesses          = file.accesses
                arr_addr            = [x.addr for x in l_accesses]
                tmp_min             = min(arr_addr)
                arr_addr            = [cur_max_addr + x - tmp_min for x in arr_addr]
                cur_max             = max(cur_max, max(arr_addr) + 1)
                # downsample if specified
                if args.down_sample != 1:
                    l_accesses = l_accesses[::args.down_sample]
                    arr_addr = arr_addr[::args.down_sample]
                if args.offset_is_mb:
                    arr_addr = [x / 1000.0 / 1000.0 for x in arr_addr]
                list_addr_normalized.extend(arr_addr)
                list_ts.extend([x.ts_ms for x in l_accesses])
                list_thr.extend([x.thread_id for x in l_accesses])
                if (len(list_phase_transitions) == 0):
                    list_phase_transitions.extend(file.phase_transitions)
            cur_max_addr = cur_max
        
        unique_threads  = sorted(list(set(list_thr)))
        # determine color mapping for plot
        norm_thr        = matplotlib.colors.Normalize(vmin=0, vmax=len(unique_threads)-1)
        norm_phases     = matplotlib.colors.Normalize(vmin=0, vmax=len(list_phase_transitions)-1)

        fig = plt.figure(figsize=(16, 9),frameon=False)
        ax = fig.gca()

        ts_min = min(list_ts)
        ts_max = max(list_ts)

        # plot accesses
        for i_thr in range(len(unique_threads)):
            thr         = unique_threads[i_thr]
            colo        = cm.gist_rainbow(norm_thr(unique_threads.index(thr)))
            indices     = [i for i, x in enumerate(list_thr) if x == thr]
            tmp_l_thr   = list(np.array(list_thr)[indices])
            tmp_ts      = list(np.array(list_ts)[indices] - ts_min)
            tmp_l_addr  = list(np.array(list_addr_normalized)[indices])

            arr_colors  = [colo for _ in tmp_l_thr]
            ax.scatter(tmp_ts, tmp_l_addr, s=1, c=arr_colors, label=f"Thread {i_thr}")

        # plot phase transitions
        for p_idx in range(len(list_phase_transitions)):
            p = list_phase_transitions[p_idx]
            if not args.plot_phases_outside:
                if p.ts_ms < ts_min or p.ts_ms > ts_max:
                    continue
            col = cm.gist_rainbow(norm_phases(p_idx))
            ax.axvline(x=p.ts_ms - ts_min, linestyle='--', color=col, label=p.name)
        
        # add legend and grid
        ax.legend(fancybox=True, shadow=False, scatterpoints=3, markerscale=2, loc='upper left', bbox_to_anchor=(1.0, 1))
        ax.minorticks_on()
        ax.grid(b=True, which='major', axis="both", linestyle='-', linewidth=1)
        ax.grid(b=True, which='minor', axis="both", linestyle='-', linewidth=0.4)
        ax.set_xlabel("Time [sec]")
        ax.set_ylabel("Offset [MB]" if args.offset_is_mb else "Offset [Address]")
        ax.set_title(f"Cummulative accesses for all callsites")
        ax.ticklabel_format(useOffset=False, style='plain')
        plt.tight_layout()

        if args.interactive:
            plt.show()
        tmp_ext = "_mb" if args.offset_is_mb else ""
        fig.savefig(os.path.join(args.destination, f"plot_all{tmp_ext}.png"), dpi='figure', format="png", bbox_inches="tight", pad_inches=0.1, facecolor='w', edgecolor='w', transparent=False)
        plt.close(fig)

    @staticmethod
    def plot_callsite(callsite, list_files, args):

        list_addr_normalized    = []
        list_ts                 = []
        list_thr                = []
        list_phase_transitions  = []
        cur_max                 = 0

        for file in list_files:
            l_accesses          = file.accesses
            arr_addr            = [x.addr for x in l_accesses]
            tmp_min             = min(arr_addr)
            arr_addr            = [x - tmp_min + cur_max for x in arr_addr]
            cur_max             = max(arr_addr)
            # downsample if specified
            if args.down_sample != 1:
                l_accesses = l_accesses[::args.down_sample]
                arr_addr = arr_addr[::args.down_sample]
            if args.offset_is_mb:
                arr_addr = [x / 1000.0 / 1000.0 for x in arr_addr]
            list_addr_normalized.extend(arr_addr)
            list_ts.extend([x.ts_ms for x in l_accesses])
            list_thr.extend([x.thread_id for x in l_accesses])
            if (len(list_phase_transitions) == 0):
                list_phase_transitions.extend(file.phase_transitions)
        
        unique_threads  = sorted(list(set(list_thr)))
        # determine color mapping for plot
        norm_thr        = matplotlib.colors.Normalize(vmin=0, vmax=len(unique_threads)-1)
        norm_phases     = matplotlib.colors.Normalize(vmin=0, vmax=len(list_phase_transitions)-1)

        fig = plt.figure(figsize=(16, 9),frameon=False)
        ax = fig.gca()

        ts_min = min(list_ts)
        ts_max = max(list_ts)
        
        # plot accesses
        for i_thr in range(len(unique_threads)):
            thr         = unique_threads[i_thr]
            colo        = cm.gist_rainbow(norm_thr(unique_threads.index(thr)))
            indices     = [i for i, x in enumerate(list_thr) if x == thr]
            tmp_l_thr   = list(np.array(list_thr)[indices])
            tmp_ts      = list(np.array(list_ts)[indices] - ts_min)
            tmp_l_addr  = list(np.array(list_addr_normalized)[indices])

            arr_colors  = [colo for _ in tmp_l_thr]
            ax.scatter(tmp_ts, tmp_l_addr, s=1, c=arr_colors, label=f"Thread {i_thr}")

        # plot phase transitions
        for p_idx in range(len(list_phase_transitions)):
            p = list_phase_transitions[p_idx]
            if not args.plot_phases_outside:
                if p.ts_ms < ts_min or p.ts_ms > ts_max:
                    continue
            col = cm.gist_rainbow(norm_phases(p_idx))
            ax.axvline(x=p.ts_ms - ts_min, linestyle='--', color=col, label=f"Phase: {p.name}")
        
        # add legend and grid
        ax.legend(fancybox=True, shadow=False, scatterpoints=3, markerscale=2, loc='upper left', bbox_to_anchor=(1.0, 1))
        ax.minorticks_on()
        ax.grid(b=True, which='major', axis="both", linestyle='-', linewidth=1)
        ax.grid(b=True, which='minor', axis="both", linestyle='-', linewidth=0.4)
        ax.set_xlabel("Time [sec]")
        ax.set_ylabel("Offset [MB]" if args.offset_is_mb else "Offset [Address]")
        ax.set_title(f"Accesses for callsite: {callsite.callsite}")
        ax.ticklabel_format(useOffset=False, style='plain')
        plt.tight_layout()

        if args.interactive:
            plt.show()
        tmp_ext = "_mb" if args.offset_is_mb else ""
        fig.savefig(os.path.join(args.destination, f"plot_callsite_{callsite.id}{tmp_ext}.png"), dpi='figure', format="png", bbox_inches="tight", pad_inches=0.1, facecolor='w', edgecolor='w', transparent=False)
        plt.close(fig)

    @staticmethod
    def plot_heatmap(callsite, list_files, args):

        list_addr_normalized    = []
        list_thr                = []
        cur_max                 = 0

        for file in list_files:
            l_accesses          = file.accesses
            arr_addr            = [x.addr for x in l_accesses]
            tmp_min             = min(arr_addr)
            arr_addr            = [x - tmp_min + cur_max for x in arr_addr]
            cur_max             = max(arr_addr)
            # downsample if specified
            if args.down_sample != 1:
                l_accesses = l_accesses[::args.down_sample]
                arr_addr = arr_addr[::args.down_sample]
            # determine page number from offset
            arr_addr = [int(x / 4096) for x in arr_addr]
            list_addr_normalized.extend(arr_addr)
            list_thr.extend([x.thread_id for x in l_accesses])
        
        fig = plt.figure(figsize=(16, 9),frameon=False)
        ax = fig.gca()
        
        unique_threads  = sorted(list(set(list_thr)))
        unique_addr     = sorted(list(set(list_addr_normalized)))
        hmap            = np.zeros((len(unique_addr), len(unique_threads)))

        # plot accesses
        for i_thr in range(len(unique_threads)):
            thr             = unique_threads[i_thr]
            idx_thr         = [i for i, x in enumerate(list_thr) if x == thr]
            tmp_arr_addr    = list(np.array(list_addr_normalized)[idx_thr])
            for i_addr in range(len(unique_addr)):
                addr        = unique_addr[i_addr]
                nr_match    = sum([1 for x in tmp_arr_addr if x == addr])
                hmap[i_addr, i_thr] = nr_match


        # build tick labels
        yticks      = np.linspace(0, len(unique_addr)-1, 12, dtype=int)
        yticklabels = [unique_addr[idx] for idx in yticks]
        if args.offset_is_mb:
            yticklabels = ["{:.2f}".format(unique_addr[idx]*4096/1000.0/1000.0) for idx in yticks]

        cmap = matplotlib.colors.LinearSegmentedColormap.from_list("", ["blue", "white"])
        # hmap[hmap==0]=np.nan
        sax = sns.heatmap(hmap, ax=ax, cmap=cmap, cbar_kws={'label': '# Accesses'})
        sax.set_yticks(yticks)
        sax.set_yticklabels(yticklabels)
        
        # add legend and grid
        ax.grid(b=True, which='major', axis="both", linestyle='-', linewidth=1)
        ax.set_xlabel("Threads")
        ax.set_ylabel("Page Offset [MB]" if args.offset_is_mb else "Page Number")
        ax.set_title(f"Heatmap for callsite: {callsite.callsite}\n")
        plt.tight_layout()

        if args.interactive:
            plt.show()
        tmp_ext = "_mb" if args.offset_is_mb else ""
        fig.savefig(os.path.join(args.destination, f"plot_heatmap_{callsite.id}{tmp_ext}.png"), dpi='figure', format="png", bbox_inches="tight", pad_inches=0.1, facecolor='w', edgecolor='w', transparent=False)
        plt.close(fig)