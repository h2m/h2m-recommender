import logging
logging.basicConfig(format='%(asctime)-3s %(name)-30s %(levelname)-10s %(message)s', level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')

def get_my_logger(name) -> logging.Logger:
    return logging.getLogger(name)