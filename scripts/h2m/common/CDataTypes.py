import os
import csv
from enum import Enum
import json
import numpy as np

class EAggregateBy(Enum):
    CALLSITE = 1
    CALLSTACK = 2
    ALLOCATION = 3

class CConfig():

    def __init__(self) -> None:
        self.source = None
        self.destination = None
        self.dest_file_postfix = None
        self.use_numamma = True
        self.aggregate_by = EAggregateBy.ALLOCATION.value
        self.gen_stats = False
        self.gen_traits = False
        self.gen_trace = True
        self.threshold_write_only = 0.9
        self.approximate_writes_to_memory = True
        self.memory_cfg = None
        self.nr_threads = False
        self.filter_callsite_ids: str = None

        self.avg_weight_read_mem = 0

        self.cap_limit_MB_idx_0 = None
        self.cap_limit_MB_idx_1 = None
        self.cap_limit_MB_idx_2 = None

        self.class_recommender       = "CTraitRecommender_TopN_V1"
        self.class_alloc_stats       = "CAllocationStats_V1"
        self.class_aggregation_stats = "CAggregationStats_V1"
    
    def set_config(self, args):
        attr_names = dir(self)
        for x in attr_names:
            if x.startswith("_"):
                continue
            if hasattr(args, x):
                exec(f"self.{x} = args.{x}")

class CMemoryKind():
    
    def __init__(self) -> None:
        self.idx = -1
        self.name = ""
        self.is_mem_space_low_lat = False
        self.is_mem_space_hbw = False
        self.is_mem_space_large_cap = False
        self.capacity_MB = 1e9
        self.scaling_threads = None
        self.scaling_bandwidth_read_only_MBs = None
        self.scaling_bandwidth_read_write_MBs = None
        self.scaling_latency_read_only_ns = None
        self.scaling_latency_read_only_bandwidth_disturbance_MBs = None
        self.scaling_latency_read_write_ns = None
        self.scaling_latency_read_write_bandwidth_disturbance_MBs = None

        self.tmp_cap_limit_MB = None # used to keep track of remaining capacity
    
    def load_from_json(self, data) -> None:
        self.idx                                                    = data["idx"]
        self.name                                                   = data["name"]
        self.is_mem_space_low_lat                                   = data["is_mem_space_low_lat"]
        self.is_mem_space_hbw                                       = data["is_mem_space_hbw"]
        self.is_mem_space_large_cap                                 = data["is_mem_space_large_cap"]
        self.capacity_MB                                            = data["capacity_MB"]
        self.scaling_threads                                        = data["scaling_threads"]
        self.scaling_bandwidth_read_only_MBs                        = data["scaling_bandwidth_read_only_MBs"]
        self.scaling_bandwidth_read_write_MBs                       = data["scaling_bandwidth_read_write_MBs"]
        self.scaling_latency_read_only_ns                           = data["scaling_latency_read_only_ns"]
        self.scaling_latency_read_only_bandwidth_disturbance_MBs    = data["scaling_latency_read_only_bandwidth_disturbance_MBs"]
        self.scaling_latency_read_write_ns                          = data["scaling_latency_read_write_ns"]
        self.scaling_latency_read_write_bandwidth_disturbance_MBs   = data["scaling_latency_read_write_bandwidth_disturbance_MBs"]

class CMemoryCopyBW():
    def __init__(self) -> None:
        self.idx_from = -1
        self.idx_to = -1
        self.size_bytes = None
        self.bandwidth_MBs = None

    def load_from_json(self, data) -> None:
        self.idx_from       = data["idx_from"]
        self.idx_to         = data["idx_to"]
        self.size_bytes     = data["size_bytes"]
        self.bandwidth_MBs  = data["bandwidth_MBs"]
    
    def get_bw_for_byte_size(self, size_bytes) -> float:
        cur_idx = 0
        for idx in range(1,len(self.size_bytes)):
            if self.size_bytes[idx] <= size_bytes:
                cur_idx = idx
            else:
                break
        return self.bandwidth_MBs[cur_idx]

class CMemoryInfo():
    memory_kinds: list[CMemoryKind]
    copy_bw: list[CMemoryCopyBW]

    def __init__(self) -> None:
        self.memory_kinds = []
        self.copy_bw = []

    @staticmethod
    def load_json_file(path_file) -> 'CMemoryInfo':
        ret = CMemoryInfo()

        with open(path_file, 'r') as f:
            data = json.load(f)
        
        data_ms = data["memory_spaces"]
        for x in data_ms:
            mem_space = CMemoryKind()
            mem_space.load_from_json(x)
            ret.memory_kinds.append(mem_space)
        
        data_copy = data["copy_bandwidth"]
        for x in data_copy:
            obj_cp = CMemoryCopyBW()
            obj_cp.load_from_json(x)
            ret.copy_bw.append(obj_cp)
        
        return ret

    def update_info(self, cfg: CConfig):
        for k in self.memory_kinds:
            val_overwrite_limit = eval(f"cfg.cap_limit_MB_idx_{k.idx}")
            if val_overwrite_limit is not None:
                k.capacity_MB = val_overwrite_limit
            k.tmp_cap_limit_MB = k.capacity_MB

class CPhaseTransition():

    def __init__(self):
        self.id         = -1
        self.name       = ""
        self.callsite   = None
        self.ts_ms      = -1

class CPhase():

    def __init__(self, id, name, ts_ms_start, ts_ms_end):
        self.id             = id
        self.name           = name
        self.ts_ms_start    = ts_ms_start
        self.ts_ms_end      = ts_ms_end
        # assumption: time stamps in ns
        self.duration_ms    = (ts_ms_end - ts_ms_start)
        self.id_trans_start = -1
        self.id_trans_end   = -1

    def reprJSON(self):
        return dict(
            id=self.id,
            name=self.name,
            ts_ms_start=self.ts_ms_start,
            ts_ms_end=self.ts_ms_end,
            duration_ms=self.duration_ms,
            id_trans_start=self.id_trans_start,
            id_trans_end=self.id_trans_end
        )

class CDataAccess():
    
    def parse_asan(self, str_line):
        spl             = str_line.split(";")
        self.ts_ms      = float(spl[0])
        self.thread_id  = int(spl[1])
        self.addr       = int(spl[3])
        self.is_write   = int(spl[5])
        self.mem_level  = ""
        self.weight     = 0
    
    def parse_numamma(self, str_line):
        spl             = [x.strip() for x in str_line.split()]
        self.thread_id  = int(spl[0])
        self.ts_ms      = float(spl[1]) / 1000.0 / 1000.0
        self.object_id  = int(spl[2])
        self.offset     = int(spl[3])
        self.mem_level  = spl[4]
        self.weight     = float(spl[5])
        self.is_write   = int(spl[6] == "w")

        # TODO: can/should we calculate addr based on offset? unification of asan/numamma?
        #self.addr       = int(spl[2])

class CDataAccesses():

    tmp_accs: list[CDataAccess]
    
    def __init__(self) -> None:
        self.is_write = np.array([])
        self.weight = np.array([])
        self.thread_id = np.array([])
        self.ts_ms = np.array([])
        self.offset = np.array([])
        self.object_id = np.array([])
        self.mem_level = np.array([])

        self.tmp_accs = []
    
    def extract(self, condition) -> 'CDataAccesses':
        ret = CDataAccesses()
        ret.is_write = self.is_write[condition]
        ret.weight = self.weight[condition]
        ret.thread_id = self.thread_id[condition]
        ret.ts_ms = self.ts_ms[condition]
        ret.offset = self.offset[condition]
        ret.object_id = self.object_id[condition]
        ret.mem_level = self.mem_level[condition]
        return ret

    def addTo(self, other: 'CDataAccesses') -> None:        
        self.is_write = np.append(self.is_write, other.is_write)
        self.weight = np.append(self.weight, other.weight)
        self.thread_id = np.append(self.thread_id, other.thread_id)
        self.ts_ms = np.append(self.ts_ms, other.ts_ms)
        self.offset = np.append(self.offset, other.offset)
        self.object_id = np.append(self.object_id, other.object_id)
        self.mem_level = np.append(self.mem_level, other.mem_level)
    
    def addAcc(self, acc: CDataAccess):
        self.tmp_accs.append(acc)

    def transformAccs(self):
        self.is_write = np.append(self.is_write, [x.is_write for x in self.tmp_accs])
        self.weight = np.append(self.weight, [x.weight for x in self.tmp_accs])
        self.thread_id = np.append(self.thread_id, [x.thread_id for x in self.tmp_accs])
        self.ts_ms = np.append(self.ts_ms, [x.ts_ms for x in self.tmp_accs])
        self.offset = np.append(self.offset, [x.offset for x in self.tmp_accs])
        self.object_id = np.append(self.object_id, [x.object_id for x in self.tmp_accs])
        self.mem_level = np.append(self.mem_level, [x.mem_level for x in self.tmp_accs])
        self.tmp_accs.clear()

class CAllocation():

    accesses: list[CDataAccess]
    daccesses: CDataAccesses

    def __init__(self, orig=None) -> None:
        self.id                 = None
        self.aggregation_id     = None
        self.address_hex        = None
        self.address            = None
        self.size_mb            = 0
        self.date_alloc_ms      = -1
        self.date_dealloc_ms    = float("inf")
        self.callstack_rip      = None
        self.callstack_offsets  = None
        self.callsite_rip       = None
        self.callsite_str       = None
        self.daccesses          = CDataAccesses()
        self.prefetch           = 0
        
        # ASan specific
        self.sampling_factor    = 1
        self.data_type_size     = 8 # double default

        # field to save stats if calculation already performed once
        self.stats              = None

        if orig is not None:
            self.id                 = orig.id
            self.aggregation_id     = orig.aggregation_id
            self.address_hex        = orig.address_hex
            self.address            = orig.address
            self.size_mb            = orig.size_mb
            self.date_alloc_ms      = orig.date_alloc_ms
            self.date_dealloc_ms    = orig.date_dealloc_ms
            self.callstack_rip      = orig.callstack_rip
            self.callstack_offsets  = orig.callstack_offsets
            self.callsite_rip       = orig.callsite_rip
            self.callsite_str       = orig.callsite_str
            # Note: do not copy accesses
            
            self.sampling_factor    = orig.sampling_factor
            self.data_type_size     = orig.data_type_size
    
    def reprJSON(self):
        return dict(
            id=self.id, 
            aggregation_id=self.aggregation_id, 
            size_mb=self.size_mb,
            date_alloc_ms=self.date_alloc_ms,
            date_dealloc_ms=self.date_dealloc_ms
        )

class CMemoryLevels():
    MEM_LEVELS_CACHE_READ   = ["L1_Hit", "L2_Hit", "L3_Hit", "NA"]
    MEM_LEVELS_CACHE_WRITE  = ["L1_Hit", "L2_Hit", "L3_Hit", "NA"]

class CAggregation():
    def __init__(self):
        self.callstack_rip          = None
        self.callstack_offsets      = None
        self.callsite_rip           = None
        self.callsite_str           = None
        self.id                     = -1
        self.callsite_group_id      = -1
        self.callstack_group_id     = -1
        self.size_mb                = -1
        self.min_alloc_date         = -1
        self.max_dealloc_date       = -1

        # try to minimize number of statistic calculation as much as possible
        self.stats                  = None

    def reprJSON(self):
        return dict(
            id=self.id, 
            callsite_group_id=self.callsite_group_id, 
            callstack_group_id=self.callstack_group_id,
            callstack_rip=self.callstack_rip,
            callstack_offsets=self.callstack_offsets,
            callsite_rip=self.callsite_rip,
            callsite_str=self.callsite_str,
            size_mb=self.size_mb,
            min_alloc_date=self.min_alloc_date,
            max_dealloc_date=self.max_dealloc_date
        )

class CParsingResult():
    
    allocations:        list[CAllocation]
    phase_transitions:  list[CPhaseTransition]
    phases:             list[CPhase]

    def __init__(self) -> None:
        self.phase_transitions  = []
        self.phases             = []
        self.allocations        = []

class CDataContainer():

    allocations:            list[CAllocation]
    phase_transitions:      list[CPhaseTransition]
    phases:                 list[CPhase]
    map_aggregation_allocs: dict[CAggregation, list[CAllocation]]
    map_phase_accesses:     dict[int, dict[CAggregation, list[CAllocation]]]

    def __init__(self, parsed_data: CParsingResult) -> None:
        self.allocations        = sorted(parsed_data.allocations, key = lambda x: x.date_alloc_ms)
        self.phase_transitions  = parsed_data.phase_transitions
        self.phases             = parsed_data.phases
    
    def aggregate_allocations(self, cfg: CConfig) -> None:

        unique_callsites    = sorted(list(set([x.callsite_rip for x in self.allocations])))
        unique_callstacks   = sorted(list(set([x.callstack_offsets for x in self.allocations])))
        unique_alloc_ids    = sorted(list(set([x.id for x in self.allocations])))

        if cfg.aggregate_by == EAggregateBy.CALLSITE.value:
            unique_items = unique_callsites
        elif cfg.aggregate_by == EAggregateBy.CALLSTACK.value:
            unique_items = unique_callstacks
        elif cfg.aggregate_by == EAggregateBy.ALLOCATION.value:
            unique_items = unique_alloc_ids
        else:
            raise NotImplementedError("Invaid aggregate_by parameter")

        self.map_aggregation_allocs = {}
        ctr_id = 0

        for aggr in unique_items:
            if cfg.aggregate_by == EAggregateBy.CALLSITE.value:
                tmp_allocs = [x for x in self.allocations if x.callsite_rip == aggr]
            elif cfg.aggregate_by == EAggregateBy.CALLSTACK.value:
                tmp_allocs = [x for x in self.allocations if x.callstack_offsets == aggr]
            elif cfg.aggregate_by == EAggregateBy.ALLOCATION.value:
                tmp_allocs = [x for x in self.allocations if x.id == aggr]

            cur_id = ctr_id
            callsite_grp_id  = unique_callsites.index(tmp_allocs[0].callsite_rip)
            callstack_grp_id = unique_callstacks.index(tmp_allocs[0].callstack_offsets)

            aggr_obj                                = CAggregation()
            aggr_obj.id                             = cur_id
            aggr_obj.callsite_group_id              = callsite_grp_id
            aggr_obj.callstack_group_id             = callstack_grp_id
            aggr_obj.callstack_rip                  = tmp_allocs[0].callstack_rip
            aggr_obj.callstack_offsets              = tmp_allocs[0].callstack_offsets
            aggr_obj.callsite_rip                   = tmp_allocs[0].callsite_rip
            aggr_obj.callsite_str                   = tmp_allocs[0].callsite_str
            aggr_obj.size_mb                        = sum([x.size_mb for x in tmp_allocs])
            aggr_obj.min_alloc_date                 = min([x.date_alloc_ms for x in tmp_allocs])
            aggr_obj.max_dealloc_date               = max([x.date_dealloc_ms for x in tmp_allocs])

            # set corresponding aggr id for single allocations
            for al in tmp_allocs:
                al.aggregation_id = aggr_obj.id

            self.map_aggregation_allocs[aggr_obj]   = tmp_allocs
            ctr_id                                  += 1
    
    def group_accesses_by_phase(self) -> None:
        self.map_phase_accesses = {}

        if len(self.phases) == 0:
            ph = CPhase(-1, "Complete Program", -1, -1)
            self.phases.append(ph)
            self.map_phase_accesses[ph.id] = self.map_aggregation_allocs
            return

        for ph in self.phases:
            new_map_aggr = {}
            for aggr, l_allocs in self.map_aggregation_allocs.items():
                new_allocs = []
                for f in l_allocs:
                    # only remove allocations that are not overlapping in any way, keep the rest
                    if f.date_dealloc_ms < ph.ts_ms_start or f.date_alloc_ms > ph.ts_ms_end:
                        continue
                    
                    filt = (f.daccesses.ts_ms >= ph.ts_ms_start) & (f.daccesses.ts_ms <= ph.ts_ms_end)
                    tmp_acc = f.daccesses.extract(filt)

                    tmp = CAllocation(f)
                    tmp.daccesses = tmp_acc
                    new_allocs.append(tmp)
                
                # set map entry for aggregation
                if len(new_allocs) > 0:
                    new_map_aggr[aggr] = new_allocs
            
            # set map entry for phase
            if len(new_map_aggr) > 0:
                self.map_phase_accesses[ph.id] = new_map_aggr
    
    def filter_out_callsites(self, cfg: CConfig, ids_to_filter: list[int]) -> None:
        filter_aggr         = []
        filter_alloc_ids    = []

        for aggr, allocs in self.map_aggregation_allocs.items():
            if aggr.callsite_group_id in ids_to_filter:
                filter_aggr.append(aggr)
                for al in allocs:
                    filter_alloc_ids.append(al.id)
        
        # remove allocations
        self.allocations = [x for x in self.allocations if x.id not in filter_alloc_ids]
        # remove aggregations
        for filt in filter_aggr:
            self.map_aggregation_allocs.pop(filt)

    def get_all_accesses(self) -> CDataAccesses:
        ret = CDataAccesses()
        for a in self.allocations:
            ret.addTo(a.daccesses)
        return ret

    def get_all_accesses_for_phase(self, phase_id: int) -> CDataAccesses:
        ret = CDataAccesses()
        if phase_id in self.map_phase_accesses.keys():
            for aggr in self.map_phase_accesses[phase_id].keys():
                for f in self.map_phase_accesses[phase_id][aggr]:
                    ret.addTo(f.daccesses)
        return ret
    
    @staticmethod
    def get_all_accesses_for_custom_data(data: dict[CAggregation, list[CAllocation]]) -> CDataAccesses:
        ret = CDataAccesses()
        for _, allocs in data.items():
            for f in allocs:
                ret.addTo(f.daccesses)
        return ret
    
    def get_allocs_for_aggregation(self, aggr: CAggregation) -> list[CAllocation]:
        if aggr in self.map_aggregation_allocs.keys():
            return self.map_aggregation_allocs[aggr]
        return []

    def get_allocs_for_phase_and_aggregation(self, phase_id: int, aggr: CAggregation) -> list[CAllocation]:
        if phase_id in self.map_phase_accesses.keys() and aggr in self.map_phase_accesses[phase_id].keys():
            return self.map_phase_accesses[phase_id][aggr]
        return []
    
    def get_accesses_for_phase_and_aggregation(self, phase_id: int, aggr: CAggregation) -> CDataAccesses:
        ret = CDataAccesses()
        if phase_id in self.map_phase_accesses.keys() and aggr in self.map_phase_accesses[phase_id].keys():
            for f in self.map_phase_accesses[phase_id][aggr]:
                ret.addTo(f.daccesses)
        return ret
    
    from .CManualInstances import CManualInstances

    def output_aggregation_stats(self, cfg: CConfig, mi: CManualInstances):
        # ============================================================
        # === Summary table overall
        # ============================================================
        target_file_stats = os.path.join(cfg.destination, f"statistics_overall{cfg.dest_file_postfix}.csv")
        with open(target_file_stats, mode="w", newline='') as f_stats:
            writer_stats = csv.writer(f_stats, delimiter=';')
            header = ["id","allocation_id","callsite_group_id","callstack_group_id","min_alloc_date","max_dealloc_date","callstack_rip","callstack_offsets","callsite_rip","callsite_str"]
            for n in mi.inst_aggregation_stats.get_names():
                header.append(n)
            phase_name = "Complete Program"
            writer_stats.writerow([f" ===== Phase: 0 {phase_name} ====="])
            writer_stats.writerow(header)
            all_acc = self.get_all_accesses()
            for aggr, _ in self.map_aggregation_allocs.items():
                # get allocations
                allocs  = self.get_allocs_for_aggregation(aggr)

                # build entry line
                tmp_arr = []
                tmp_arr.append(aggr.id)
                if cfg.aggregate_by == 3 and len(allocs) > 0:
                    tmp_arr.append(allocs[0].id)
                else:
                    tmp_arr.append(-1)
                tmp_arr.append(aggr.callsite_group_id)
                tmp_arr.append(aggr.callstack_group_id)
                tmp_arr.append(aggr.min_alloc_date)
                tmp_arr.append(aggr.max_dealloc_date)
                tmp_arr.append(aggr.callstack_rip)
                tmp_arr.append(aggr.callstack_offsets)
                tmp_arr.append(aggr.callsite_rip)
                tmp_arr.append(aggr.callsite_str)
                
                stats = mi.inst_aggregation_stats.get_stats(cfg, allocs, all_acc, allocs)
                for n in mi.inst_aggregation_stats.get_names():
                    tmp_arr.append(eval(f"stats.{n}"))
                writer_stats.writerow(tmp_arr)
            writer_stats.writerow([])
        
        # ============================================================
        # === Summary table per phase
        # ============================================================
        target_file_stats = os.path.join(cfg.destination, f"statistics_per-phase{cfg.dest_file_postfix}.csv")
        with open(target_file_stats, mode="w", newline='') as f_stats:
            writer_stats = csv.writer(f_stats, delimiter=';')
            header = ["id","allocation_id","callsite_group_id","callstack_group_id","min_alloc_date","max_dealloc_date","callstack_rip","callstack_offsets","callsite_rip","callsite_str"]
            for n in mi.inst_aggregation_stats.get_names():
                header.append(n)
            for ph_id in self.map_phase_accesses.keys():
                phase_name = [x.name for x in self.phases if x.id == ph_id][0] if ph_id is not None else "Complete Program"
                writer_stats.writerow([f" ===== Phase: {ph_id} {phase_name} ====="])
                writer_stats.writerow(header)
                all_acc_phase = self.get_all_accesses_for_phase(ph_id)
                for aggr, _ in self.map_aggregation_allocs.items():
                    allocs = self.get_allocs_for_phase_and_aggregation(ph_id, aggr)
                    if len(allocs) == 0:
                        continue
                    acc_ph_cs = self.get_accesses_for_phase_and_aggregation(ph_id, aggr)
                    if len(acc_ph_cs.ts_ms) == 0:
                        continue

                    tmp_arr = []
                    tmp_arr.append(aggr.id)
                    if cfg.aggregate_by == 3:
                        tmp_arr.append(allocs[0].id)
                    else:
                        tmp_arr.append(-1)
                    tmp_arr.append(aggr.callsite_group_id)
                    tmp_arr.append(aggr.callstack_group_id)
                    tmp_arr.append(aggr.min_alloc_date)
                    tmp_arr.append(aggr.max_dealloc_date)
                    tmp_arr.append(aggr.callstack_rip)
                    tmp_arr.append(aggr.callstack_offsets)
                    tmp_arr.append(aggr.callsite_rip)
                    tmp_arr.append(aggr.callsite_str)

                    allocs_aggr_complete = self.get_allocs_for_aggregation(aggr)
                    stats = mi.inst_aggregation_stats.get_stats(cfg, allocs, all_acc_phase, allocs_aggr_complete)
                    for n in mi.inst_aggregation_stats.get_names():
                        tmp_arr.append(eval(f"stats.{n}"))
                    writer_stats.writerow(tmp_arr)
                writer_stats.writerow([])

        # for now that is enough
        return
    
        # ============================================================
        # === Detailed per-property tables overall
        # ============================================================
        # only run this section if there are multiple phases
        base_info_names = mi.inst_aggregation_stats.get_base_info_names()
        header = ["id","callstack_rip","callstack_offsets","callsite_rip","callsite_str"]
        header.extend(base_info_names)
        header.append("Complete Program")
            
        target_file_stats = os.path.join(cfg.destination, "statistics_per-property_overall.csv")
        with open(target_file_stats, mode="w", newline='') as f_stats:
            writer_stats = csv.writer(f_stats, delimiter=';')

            tmp_cache = {}
            global_acc = self.get_all_accesses()

            # skip base infos as already available
            for prop in mi.inst_aggregation_stats.get_names():
                if prop in base_info_names:
                    continue
            
                writer_stats.writerow([f"========== {prop} =========="])
                writer_stats.writerow(header)
                for aggr, all_allocs in self.map_aggregation_allocs.items():
                    tmp_arr = []
                    tmp_arr.append(aggr.id)
                    tmp_arr.append(aggr.callstack_rip)
                    tmp_arr.append(aggr.callstack_offsets)
                    tmp_arr.append(aggr.callsite_rip)
                    tmp_arr.append(aggr.callsite_str)

                    # add base info
                    all_stats = mi.inst_aggregation_stats.get_stats(cfg, all_allocs, global_acc, all_allocs)
                    for bn in base_info_names:
                        tmp_arr.append(eval(f"all_stats.{bn}"))
                    tmp_arr.append(eval(f"all_stats.{prop}"))

                    writer_stats.writerow(tmp_arr)
                writer_stats.writerow([])

        # ============================================================
        # === Detailed per-property tables accross phases
        # ============================================================
        # Note: currently always create this file
        if len(self.phases) > -1:
            # only run this section if there are multiple phases
            base_info_names = mi.inst_aggregation_stats.get_base_info_names()
            header = ["id","callstack_rip","callstack_offsets","callsite_rip","callsite_str"]
            header.extend(base_info_names)
            for ph_id in sorted(list(self.map_phase_accesses.keys())):
                phase_name = [x.name for x in self.phases if x.id == ph_id][0] if ph_id is not None else "Complete Program"
                header.append(phase_name)
            
            target_file_stats = os.path.join(cfg.destination, "statistics_per-property_per-phase.csv")
            with open(target_file_stats, mode="w", newline='') as f_stats:
                writer_stats = csv.writer(f_stats, delimiter=';')

                tmp_cache = {}

                # skip base infos as already available
                for prop in mi.inst_aggregation_stats.get_names():
                    if prop in base_info_names:
                        continue
                
                    writer_stats.writerow([f"========== {prop} =========="])
                    writer_stats.writerow(header)
                    for aggr, all_allocs in self.map_aggregation_allocs.items():
                        tmp_arr = []
                        tmp_arr.append(aggr.id)
                        tmp_arr.append(aggr.callstack_rip)
                        tmp_arr.append(aggr.callstack_offsets)
                        tmp_arr.append(aggr.callsite_rip)
                        tmp_arr.append(aggr.callsite_str)

                        # add base info
                        all_stats = mi.inst_aggregation_stats.get_stats(cfg, all_allocs, None)
                        for bn in base_info_names:
                            tmp_arr.append(eval(f"all_stats.{bn}"))

                        # add info per phase
                        for ph_id in sorted(list(self.map_phase_accesses.keys())):
                            cur_key = (ph_id, aggr)
                            if cur_key in tmp_cache:
                               stats =  tmp_cache[cur_key]
                            else:
                                allocs = self.get_allocs_for_phase_and_aggregation(ph_id, aggr)
                                all_acc_phase = self.get_all_accesses_for_phase(ph_id)
                                stats = mi.inst_aggregation_stats.get_stats(cfg, allocs, all_acc_phase, all_allocs)
                                tmp_cache[cur_key] = stats
                            tmp_arr.append(eval(f"stats.{prop}"))
                        
                        writer_stats.writerow(tmp_arr)
                    writer_stats.writerow([])
