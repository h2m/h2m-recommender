import os, sys
import statistics as st
import csv

from .CDataTypes import *
from . import CLogging

class CDataParser:

    logger = CLogging.get_my_logger("DataParser")

    @staticmethod
    def __determine_phases(result: CParsingResult) -> None:
        if len(result.phase_transitions) > 0:
            # sort phase transitions first
            result.phase_transitions.sort(key=lambda x: x.ts_ms, reverse=False)
            ctr_id = 0

            for i_ph in range(len(result.phase_transitions)):
                ph_tr       = result.phase_transitions[i_ph]
                ph_tr_last  = None if i_ph == 0 else result.phase_transitions[i_ph-1]

                if ph_tr_last:
                    ph = CPhase(ctr_id, f"between {ph_tr_last.name} and {ph_tr.name}", ph_tr_last.ts_ms, ph_tr.ts_ms)
                    ph.id_trans_start   = ph_tr_last.id
                else:
                    # need to introduce AppStart phase
                    ph = CPhase(ctr_id, f"between AppStart and {ph_tr.name}", 0, ph_tr.ts_ms)
                # set end transition id
                ph.id_trans_end = ph_tr.id
                
                # append phase to list of phases
                result.phases.append(ph)
                ctr_id += 1
            
            # add last phase until AppEnd
            last_ph = result.phase_transitions[-1]
            ph = CPhase(ctr_id, f"between {last_ph.name} and AppEnd", last_ph.ts_ms, sys.maxsize)
            # set start transition id
            ph.id_trans_start = ph_tr.id
            # append phase to list of phases
            result.phases.append(ph)
            ctr_id += 1

    @staticmethod
    def __parse_asan(cfg: CConfig) -> CParsingResult:
        raise NotImplementedError("Parsing ASan dumps not implemented yet")

    @staticmethod
    def __parse_numamma(cfg: CConfig) -> CParsingResult:
        result        = CParsingResult()
        file_mem_objs = os.path.join(cfg.source, "all_memory_objects.dat")
        file_mem_accs = os.path.join(cfg.source, "all_memory_accesses.dat")
        file_phases   = os.path.join(cfg.source, "h2m_dump_phase_transitions.csv")
        
        ##################################################
        ### Step 1: check if files are existing
        ##################################################
        if not os.path.exists(file_mem_objs):
            CDataParser.logger.error("File \"all_memory_objects.dat\" seems not to be existing. Please check the NumaMMA dumps")
            return None
        if not os.path.exists(file_mem_accs):
            CDataParser.logger.error("File \"all_memory_accesses.dat\" seems not to be existing. Please check the NumaMMA dumps")
            return None

        ##################################################
        ### Step 2: parse mem objects / allocations
        ##################################################
        CDataParser.logger.info("Parsing memory objects / allocations")

        map_allocations: dict[int,CAllocation] = {}

        with open(file_mem_objs) as f: lines = [x.strip() for x in list(f)]
        for line in lines:
            # filter out stack accesses
            if "[stack]" in line:
                continue
            if "NULL" in line:
                continue
            if line.startswith("#object_id"):
                continue

            spl = [x.strip() for x in line.split("\t")]
            cur_alloc                   = CAllocation()
            cur_alloc.id                = int(spl[0])
            cur_alloc.callsite_rip      = spl[7]

            # filter out invalid allocations
            if cur_alloc.callsite_rip == "0x0":
                continue
            # check if allocation already contained (bug: some NumaMMA outputs contain duplicate information)
            if cur_alloc.id in map_allocations.keys():
                continue
            
            cur_alloc.address_hex       = spl[1]
            cur_alloc.address           = int(spl[1], 16)
            cur_alloc.size_mb           = float(spl[2]) / 1e6
            cur_alloc.date_alloc_ms     = int(float((spl[3]))) / 1000.0 / 1000.0
            cur_alloc.date_dealloc_ms   = int(float((spl[4]))) / 1000.0 / 1000.0
            cur_alloc.callstack_rip     = spl[5]
            cur_alloc.callstack_offsets = spl[6]
            cur_alloc.callsite_str      = spl[8]

            # add to list
            map_allocations[cur_alloc.id] = cur_alloc
            result.allocations.append(cur_alloc)
            
        ##################################################
        ### Step 3: parse memory accesses
        ##################################################
        CDataParser.logger.info("Parsing memory accesses for allocations")
        with open(file_mem_accs) as f: lines = [x.strip() for x in list(f)]
        for line in lines:
            if line.startswith("#"):
                continue
            acc = CDataAccess()
            acc.parse_numamma(line)

            # skip if access cannot be assigned
            alloc_found = acc.object_id in map_allocations.keys()
            if not alloc_found:
                continue
            # assign access to corresponding allocation
            cur_alloc = map_allocations[acc.object_id]
            cur_alloc.daccesses.addAcc(acc)
        
        for al in map_allocations.values():
            al.daccesses.transformAccs()
        
        ##################################################
        ### Step 4: Filter out allocations with no accesses
        ##################################################
        CDataParser.logger.info("Filter out allocations with no accesses")
        # print(len(result.allocations))
        result.allocations = [x for x in result.allocations if len(x.daccesses.ts_ms) >= 20]
        # print(len(result.allocations))

        ##################################################
        ### Step 5: Parse information about phase transitions
        ##################################################
        # only parse this information of phases should be considered
        CDataParser.logger.info("Parsing phase transition information")
        if os.path.exists(file_phases):
            with open(file_phases) as f: lines = [x.strip() for x in list(f)]
            for line in lines:
                if line.startswith("ID"):
                    continue
                # create new phase transition                
                spl      = line.split(";")
                pt       = CPhaseTransition()
                pt.id    = int(spl[0])
                pt.name  = spl[1]
                pt.ts_ms = float(spl[2]) / 1000.0 / 1000.0
                result.phase_transitions.append(pt)

        return result
    
    @staticmethod
    def parse_data(cfg: CConfig) -> CParsingResult:
        # parsing
        if cfg.use_numamma:
            result = CDataParser.__parse_numamma(cfg)
        else:
            result = CDataParser.__parse_asan(cfg)

        # determine phases
        CDataParser.__determine_phases(result)

        # calculate mean weight considering all read accesses
        sum_weight_read = 0
        num_weights = 0
        for al in result.allocations:
            for idx in range(len(al.daccesses.ts_ms)):
                if not al.daccesses.is_write[idx] and al.daccesses.mem_level[idx] not in CMemoryLevels.MEM_LEVELS_CACHE_READ and al.daccesses.weight[idx] > 0:
                    sum_weight_read += al.daccesses.weight[idx]
                    num_weights += 1
        if num_weights > 0:
            cfg.avg_weight_read_mem = float(sum_weight_read) / num_weights

        return result