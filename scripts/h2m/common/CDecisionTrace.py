import csv
import copy
import itertools
import numpy as np
from typing import Tuple

from .CDataTypes import CPhase, CDataContainer, CAggregation, CAllocation, CConfig
from .CManualInstances import CManualInstances
from .CStatisticsCache import CStatisticsCache
from .CAggregationStats_V1 import CAggregationStats_V1

class CDecisionTraceEntry():
    def __init__(self) -> None:
        self.in_fast_mem = None
        self.move_out = None
        self.stats = None

class CDecisionTrace():

    def __init__(self,
                 dc: CDataContainer,
                 cfg: CConfig,
                 mi: CManualInstances,
                 sampling_freq: int = 6000,
                 phase_based: bool = True,
                 stats_caches: CStatisticsCache = CStatisticsCache(),
                 apply_gain_decay: bool = False) -> None:

        self.dc = dc
        self.cfg = cfg
        self.instances = mi
        self.sampling_freq = sampling_freq
        self.phase_based = phase_based
        self.stats_caches = stats_caches
        self.apply_gain_decay = apply_gain_decay

        # initialize
        num_phases = len(dc.phases) if self.phase_based else 1
        self.trace = np.ndarray([num_phases, len(dc.allocations)], dtype=CDecisionTraceEntry)

        for idx_al in range(len(self.dc.allocations)):
            self.trace[0, idx_al] = CDecisionTraceEntry()
    
    def update_decision_trace(self,
                              ph_idx_start: int,
                              ph_idx_end: int,
                              data_phase_combined: dict[CAggregation, list[CAllocation]],
                              ids_in_fast_mem: list[int], 
                              ids_move_out: list[int]) -> None:

        # by default use state of last phase as basis
        if ph_idx_start > 0:
            for idx_al in range(len(self.dc.allocations)):
                self.trace[ph_idx_start, idx_al] = copy.deepcopy(self.trace[ph_idx_start-1, idx_al])
                self.trace[ph_idx_start, idx_al].stats = None

        # check whether profiling data for phase exists (that might not also be the case)
        if data_phase_combined is None:
            return

        # get all accesses for data (required for modeling)
        all_acc = CDataContainer.get_all_accesses_for_custom_data(data_phase_combined)

        # check again for individual allocations
        if all_acc.ts_ms.size == 0:
            return
        
        # get list of all allocations
        all_phase_allocations = list(itertools.chain.from_iterable([x for x in data_phase_combined.values()]))

        # get current phase object
        # ph_obj = self.dc.phases[ph_idx]

        for idx_alloc in range(len(self.dc.allocations)):
            # get allocation for phase
            alloc_base = self.dc.allocations[idx_alloc]
            alloc = [x for x in all_phase_allocations if x.id == alloc_base.id]

            # check if data for allocation present in this phase
            if len(alloc) == 0:
                continue
            alloc = alloc[0]

            # check whether statistics need to be calculated
            key = (ph_idx_start, alloc.id)
            if key in self.stats_caches.cache_alloc_stats_combined.keys():
                tmp_stats = self.stats_caches.cache_alloc_stats_combined[key]
                self.stats_caches.cache_alloc_stats_combined_hits += 1
            else:
                tmp_stats = self.instances.inst_aggregation_stats.get_stats(self.cfg, [alloc], all_acc)
                self.stats_caches.cache_alloc_stats_combined[key] = tmp_stats
                self.stats_caches.cache_alloc_stats_combined_misses += 1

            if tmp_stats.acc_total > 0:
                # apply decay depending on strategy and calculated/update expected and gain
                if self.apply_gain_decay:
                    n_phases = ph_idx_end - ph_idx_start + 1
                    # only apply decay if multiple phases are considered
                    if n_phases > 1:
                        tmp_gain = CAggregationStats_V1.calculate_gain_with_decay(
                            self.instances,
                            self.cfg,
                            self.dc,
                            self.stats_caches,
                            alloc,
                            ph_idx_start,
                            ph_idx_end,
                            all_acc)

                        # update gain in original stats object
                        tmp_stats.exp_gain = tmp_gain

                self.trace[ph_idx_start, idx_alloc].stats = tmp_stats

            self.trace[ph_idx_start, idx_alloc].in_fast_mem = alloc.id in ids_in_fast_mem

            # TODO: potentially filter out allocations that have already been deallocated
            # self.trace[ph_idx, idx_alloc].move_out = alloc.id in ids_move_out and alloc.date_dealloc_ms > ph_obj.ts_ms_end
            self.trace[ph_idx_start, idx_alloc].move_out = alloc.id in ids_move_out
    
    def write_to_file(self, outfile):
        writer_stats = csv.writer(outfile, delimiter=';')
        base_header = ["allocation_id","callsite_group_id","callstack_group_id","alloc_date","dealloc_date","callstack_rip","callstack_offsets","callsite_rip","callsite_str", "size_mb"]

        # (name, convert-to-int, need_scaling, also_print_future_allocations, force_print)
        print_future_allocs = True
        fields = [
            ("in_fast_mem", True, False, print_future_allocs, False),
            ("move_out", True, False, False, False),
            ("stats.exp_acc_time_fast", False, True, print_future_allocs, False),
            ("stats.exp_acc_time_slow", False, True, print_future_allocs, False),
            ("stats.exp_gain", False, True, print_future_allocs, False),
            ("stats.exp_transfer_time_to_fast", False, False, print_future_allocs, True),
            ("stats.exp_transfer_time_to_slow", False, False, print_future_allocs, True)
        ]

        num_phases = len(self.dc.phases) if self.phase_based else 1

        # key: allocation_idx, value: stats
        lookup_overall_stats = {}
        tmp_all_acc = self.dc.get_all_accesses()

        # output desired fields to csv format
        for fname, cvi, need_scaling, also_print_future_instances, force_print in fields:
            cur_header = copy.deepcopy(base_header)

            if self.phase_based:
                for idx_phase in range(num_phases):
                    cur_header.append(self.dc.phases[idx_phase].name)
            else:
                cur_header.append("Complete Program")

            name_ext = f" (scaled with factor {self.sampling_freq})" if need_scaling else ""
            writer_stats.writerow([f" ===== Decision/Placement Trace: {fname}{name_ext} ====="])
            writer_stats.writerow(cur_header)
            
            for idx_al in range(len(self.dc.allocations)):
                # build entry line
                tmp_arr = []

                alloc = self.dc.allocations[idx_al]
                aggr = [x for x in self.dc.map_aggregation_allocs.keys() if x.id == alloc.aggregation_id][0]

                tmp_arr.append(alloc.id)
                tmp_arr.append(aggr.callsite_group_id)
                tmp_arr.append(aggr.callstack_group_id)
                tmp_arr.append(alloc.date_alloc_ms)
                tmp_arr.append(alloc.date_dealloc_ms)
                tmp_arr.append(aggr.callstack_rip)
                tmp_arr.append(aggr.callstack_offsets)
                tmp_arr.append(aggr.callsite_rip)
                tmp_arr.append(aggr.callsite_str)
                tmp_arr.append(alloc.size_mb)

                for idx_phase in range(num_phases):
                    if self.phase_based:
                        # check whether allocation is overlapping with phase 
                        cur_phase = self.dc.phases[idx_phase]
                        overlap = not (alloc.date_dealloc_ms < cur_phase.ts_ms_start or alloc.date_alloc_ms > cur_phase.ts_ms_end)
                    else:
                        overlap = True

                    if not overlap and also_print_future_instances:
                        overlap = not (alloc.date_dealloc_ms < cur_phase.ts_ms_start)

                    # get info from trace entry
                    cur_val = None

                    # only print info if allocation overlaps with the current phase
                    # allocations that are not allocated yet or have already been freed are not interesting
                    if overlap or force_print:
                        if self.trace[idx_phase, idx_al].stats is None and force_print:
                            if idx_al not in lookup_overall_stats:
                                tmp_stats = self.instances.inst_aggregation_stats.get_stats(self.cfg, [alloc], tmp_all_acc)
                                lookup_overall_stats[idx_al] = tmp_stats
                            else:
                                tmp_stats = lookup_overall_stats[idx_al]
                            self.trace[idx_phase, idx_al].stats = tmp_stats

                        try:
                            if cvi:
                                cur_val = int(eval(f"self.trace[idx_phase, idx_al].{fname}"))
                            else:
                                # this might crash if stats = None e.g. if no accesses for that phase or data not yet allocated in that phase
                                cur_val = eval(f"self.trace[idx_phase, idx_al].{fname}")
                        except:
                            pass

                        # apply scaling with sampling freq if necessary
                        if cur_val is not None and need_scaling:
                            cur_val = cur_val * self.sampling_freq

                    tmp_arr.append(cur_val)
                writer_stats.writerow(tmp_arr)
            
            writer_stats.writerow([])