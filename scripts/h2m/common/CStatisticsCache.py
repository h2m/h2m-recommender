# from .CAggregationStats_V1 import CAggregationStats_V1
from typing import Tuple 

class CStatisticsCache:

    cache_alloc_stats_combined: dict[Tuple[int,int], object]
    cache_alloc_stats_per_phase: dict[Tuple[int,int], object]
    
    def __init__(self):
        self.cache_alloc_stats_combined = {}
        self.cache_alloc_stats_per_phase = {}
        self.cache_alloc_stats_combined_hits = 0
        self.cache_alloc_stats_combined_misses = 0
        self.cache_alloc_stats_per_phase_hits = 0
        self.cache_alloc_stats_per_phase_misses = 0

    def reset_caches(self):
        self.cache_alloc_stats_combined.clear()
        self.cache_alloc_stats_per_phase.clear()
        self.cache_alloc_stats_combined_hits = 0
        self.cache_alloc_stats_combined_misses = 0
        self.cache_alloc_stats_per_phase_hits = 0
        self.cache_alloc_stats_per_phase_misses = 0

    def print_cache_ctrs(self):
        # only used for debugging purposes
        # print(f"cache_alloc_stats_combined_hits: {self.cache_alloc_stats_combined_hits}")
        # print(f"cache_alloc_stats_combined_misses: {self.cache_alloc_stats_combined_misses}")
        # print(f"cache_alloc_stats_per_phase_hits: {self.cache_alloc_stats_per_phase_hits}")
        # print(f"cache_alloc_stats_per_phase_misses: {self.cache_alloc_stats_per_phase_misses}")
        pass