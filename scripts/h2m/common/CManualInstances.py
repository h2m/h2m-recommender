from .CAllocationStatsBase import CAllocationStatsBase
from .CAggregationStatsBase import CAggregationStatsBase
from .CDataTypes import CConfig, CMemoryInfo

class CManualInstances():

    inst_allocation_stats: CAllocationStatsBase
    inst_aggregation_stats: CAggregationStatsBase

    def __init__(self, cfg: CConfig, mem_info: CMemoryInfo) -> None:
        # dynamically load the desired implementations
        exec(f"from .{cfg.class_alloc_stats} import {cfg.class_alloc_stats}")
        self.inst_allocation_stats = eval(f"{cfg.class_alloc_stats}()")
        self.inst_allocation_stats.set_data(cfg, mem_info)

        exec(f"from .{cfg.class_aggregation_stats} import {cfg.class_aggregation_stats}")
        self.inst_aggregation_stats = eval(f"{cfg.class_aggregation_stats}()")
        self.inst_aggregation_stats.set_data(cfg, mem_info, self.inst_allocation_stats)