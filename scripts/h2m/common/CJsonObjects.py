from .CDataTypes import *
from .CTraitEntry import *

class CustomEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj,'reprJSON'):
            return obj.reprJSON()
        else:
            return json.JSONEncoder.default(self, obj)

class CJsonAllocationTraits():

    def __init__(self) -> None:
        self.allocation_id = -1
        self.prefetch = 0
        self.traits = None
    
    def reprJSON(self):
        return dict(
            allocation_id=self.allocation_id,
            prefetch=self.prefetch,
            traits=self.traits
        )

class CJsonTraitsPhase():

    def __init__(self) -> None:
        self.phase_id = -1
        self.allocation_traits  = []
    
    def reprJSON(self):
        return dict(
            phase_id=self.phase_id,
            allocation_traits=self.allocation_traits
        )

class CJsonTraitsWrapper():

    def __init__(self) -> None:
        self.aggregations = []
        self.allocations = []
        self.phases = []
        self.phase_traits = []
    
    def reprJSON(self):
        return dict(
            aggregations=self.aggregations,
            allocations=self.allocations,
            phases=self.phases,
            phase_traits=self.phase_traits
        )

    @staticmethod
    def generate_json_dump(dc: CDataContainer, map_traits: dict[CPhase, dict[CAllocation, list[CTraitEntry]]]) -> str:
        obj              = CJsonTraitsWrapper()
        obj.aggregations = sorted(dc.map_aggregation_allocs.keys(), key = lambda x: x.id)
        obj.allocations  = sorted(dc.allocations, key = lambda x: x.date_alloc_ms)
        obj.phases       = sorted([x for x in map_traits.keys()], key = lambda x: x.id)

        for ph in obj.phases:
            tmp_ph = CJsonTraitsPhase()
            tmp_ph.phase_id = ph.id
            tmp_map = map_traits[ph]

            for alloc, traits in tmp_map.items():
                tmp_al = CJsonAllocationTraits()
                tmp_al.allocation_id = alloc.id
                tmp_al.traits = traits
                tmp_al.prefetch = alloc.prefetch
                tmp_ph.allocation_traits.append(tmp_al)

            obj.phase_traits.append(tmp_ph)

        # serializing and writing json
        json_object = json.dumps(obj, indent=4, cls=CustomEncoder)
        return json_object