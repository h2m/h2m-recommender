import argparse
import os, sys
import time

# append path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)))))

from h2m.common.CDataParser import *
from h2m.common.CLogging import *
from h2m.trait_recommender.CTraitRecommendationSystem import *
from h2m.common.CManualInstances import CManualInstances

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Parses and summarizes execution time measurement results")
    parser.add_argument("--source",               required=True,  type=str,   metavar="<source folder>", help=f"source folder containing outputs")
    parser.add_argument("--memory_cfg",           required=True,  type=str,   metavar="<mem-cfg>", default="", help=f"Path to JSON config file containing memory configuration of target system")
    parser.add_argument("--destination",          required=False, type=str,   metavar="<dest folder>", default=None, help=f"destination folder where resulting data and plots will be stored")    
    parser.add_argument("--dest_file_postfix",    required=False, type=str,   metavar="<string>", default="", help=f"filename postfix to be used for traits and statistics")
    parser.add_argument("--use_numamma",          required=False, type=int,   default=1, help=f"Whether dataset is based on NumaMMA dumps. Otherwise it is based on LLVM ASan dumps.")
    parser.add_argument("--aggregate_by",         required=False, type=int,   default=3, help=f"Whether to aggregate allocations/objects by callsite(1), complete callstack(2) or per allocation(3). Default is per allocation(3)")
    parser.add_argument("--gen_stats",            required=False, type=int,   default=1, help=f"Whether to generate statistics per aggregation")
    parser.add_argument("--gen_traits",           required=False, type=int,   default=1, help=f"Whether to generate trait sets")
    parser.add_argument("--gen_trace",            required=False, type=int,   default=1, help=f"Whether to generate decision trace resulting from optimization")
    parser.add_argument("--nr_threads",           required=False, type=int,   default=1, help=f"Number of desired threads to run the application with. Potentially used for performance modeling.")
    parser.add_argument("--filter_callsite_ids",  required=False, type=str,   default=None, help=f"Option to filter out complete callsite ids before generating statistics and placement decisions. Expected: Comma separated list.")
    
    # parser.add_argument("--class_recommender",    required=False, type=str,   default="CTraitRecommender_TopN", help=f"File and class name of the trait recommender implementation to use.")
    parser.add_argument("--class_recommender",    required=False, type=str,   default="CTraitRecommender_Knapsack", help=f"File and class name of the trait recommender implementation to use.")
    parser.add_argument("--class_alloc_stats",    required=False, type=str,   default="CAllocationStats_V1", help=f"File and class name of the allocation stats implementation to use.")
    parser.add_argument("--class_aggregation_stats", required=False, type=str,   default="CAggregationStats_V1", help=f"File and class name of the aggregation stats implementation to use.")
    
    parser.add_argument("--cap_limit_MB_idx_0",   required=False, type=float,  default=None, help=f"Possibility to overwrite capacity limit for mem-space")
    parser.add_argument("--cap_limit_MB_idx_1",   required=False, type=float,  default=None, help=f"Possibility to overwrite capacity limit for mem-space")
    parser.add_argument("--cap_limit_MB_idx_2",   required=False, type=float,  default=None, help=f"Possibility to overwrite capacity limit for mem-space")

    logger = CLogging.get_my_logger("Main")
    args   = parser.parse_args()

    args.gen_stats              = args.gen_stats==1
    args.gen_traits             = args.gen_traits==1
    args.gen_trace              = args.gen_trace==1
    args.use_numamma            = args.use_numamma==1

    # ========== DEBUGGING ==========
    # main_h2m_dir = "C:\\J.Klinkenberg.Local\\repos\\hpc-projects\\h2m"
    # # main_h2m_dir = "F:\\repos\\hpc-projects\\h2m"
    # # args.source = f"{main_h2m_dir}\\h2m-experiments\\app-data-placement\\XSBench\\results_numamma_alarm_100_sr_1000"
    # args.source = f"{main_h2m_dir}\\h2m-results\\2024-04-26_Technical-Sync\\SPEC_351\\results_numamma_sr_6000"
    # args.memory_cfg = f"{main_h2m_dir}\\h2m-recommender\\configs\\icelake-optane.json"
    # args.gen_stats = False
    # # args.aggregate_by = EAggregateBy.ALLOCATION.value
    # args.gen_traits = True
    # args.gen_trace = True
    # args.aggregate_by = EAggregateBy.CALLSTACK.value
    # args.cap_limit_MB_idx_0 = 553
    # args.nr_threads = 16
    # # args.filter_callsite_ids = "0,33,34,35,36,37,38,39,40,41,46,47,48,49,50"
    # os.environ["KS_ARR_NEXT_N_PHASES"] = "7,8,9,10,12,all"
    # os.environ["KS_OBJ_FNC_VARIANT"] = "GAIN_AND_TRANSFER_TIME_DECAY"
    # args.dest_file_postfix = "_new"
    # ===============================

    if not os.path.exists(args.source):
        logger.error(f"Source folder path \"{args.source}\" does not exist")
        sys.exit(1)
    if args.gen_traits and not os.path.exists(args.memory_cfg):
        logger.error(f"Memory configuration JSON file \"{args.memory_cfg}\" does not exist")
        sys.exit(1)

    # save results in source folder
    if args.destination is None:
        args.destination = os.path.join(args.source, "recommendations")

    if not os.path.exists(args.destination):
        os.makedirs(args.destination)
    
    # measure how long it took
    elapsed_time = time.time()
    
    # finally create configuration based on args
    cfg = CConfig()
    cfg.set_config(args)

    # load memory info from JSON file
    mem_info = CMemoryInfo.load_json_file(cfg.memory_cfg)
    mem_info.update_info(cfg)

    # instantiate manual implementations for stats calculation
    mi = CManualInstances(cfg, mem_info)

    # parsing data
    logger.info(f"Parse datasets ...")
    parse_data = CDataParser.parse_data(cfg)

    # init data container
    dc = CDataContainer(parse_data)
    
    # grouping and processing data
    logger.info(f"Aggregate allocations by callsite/callstack/allocation ...")
    dc.aggregate_allocations(cfg)

    # filter out callsites if configured
    if cfg.filter_callsite_ids is not None:
        logger.info(f"Filtering out callsite ids {cfg.filter_callsite_ids} ...")
        tmp_spl = cfg.filter_callsite_ids.split(",")
        ids_to_filter = [int(x.strip()) for x in tmp_spl]
        dc.filter_out_callsites(cfg, ids_to_filter)
    
    # mapping accesses to phases
    logger.info(f"Map accesses by phase ...")
    dc.group_accesses_by_phase()

    if cfg.gen_stats:
        logger.info(f"Output aggregation statistics ...")
        dc.output_aggregation_stats(cfg, mi)
    
    if cfg.gen_traits:
        logger.info(f"Creating trait recommendations ...")

        # build lookup for memory kinds and spaces
        map_mem_spaces: dict[ETraitValue, CMemoryKind] = {}
        for k in mem_info.memory_kinds:
            if k.is_mem_space_hbw:
                map_mem_spaces[ETraitValue.MEM_SPACE_HBW] = k
            if k.is_mem_space_large_cap:
                map_mem_spaces[ETraitValue.MEM_SPACE_LARGE_CAP] = k
            if k.is_mem_space_low_lat:
                map_mem_spaces[ETraitValue.MEM_SPACE_LOW_LAT] = k

        # finally generate traits
        rec = CTraitRecommendationSystem(dc, cfg, memory_info=mem_info, mi=mi)
        rec.generate(map_mem_spaces)
    
    elapsed_time = time.time() - elapsed_time

    logger.info(f"Completed in {elapsed_time} seconds")