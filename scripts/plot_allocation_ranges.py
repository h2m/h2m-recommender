import argparse
import os, sys
import matplotlib
import matplotlib.pyplot as plt

# append path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)))))

from h2m.common.CDataParser import *
from h2m.common.CLogging import *
from h2m.trait_recommender.CTraitRecommendationSystem import *
from h2m.common.CManualInstances import CManualInstances

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Parses and summarizes execution time measurement results")
    parser.add_argument("--source",               required=True,  type=str,   metavar="<source folder>", help=f"source folder containing outputs")
    parser.add_argument("--destination",          required=False, type=str,   metavar="<dest folder>", default=None, help=f"destination folder where resulting data and plots will be stored")    
    parser.add_argument("--use_numamma",          required=False, type=int,   default=1, help=f"Whether dataset is based on NumaMMA dumps. Otherwise it is based on LLVM ASan dumps.")
    
    logger = CLogging.get_my_logger("Main")
    args   = parser.parse_args()
    args.use_numamma = args.use_numamma==1

    # ========== DEBUGGING ==========
    # main_h2m_dir = "C:\\J.Klinkenberg.Local\\repos\\hpc-projects\\h2m"
    # # main_h2m_dir = "F:\\repos\\hpc-projects\\h2m"
    # args.source = f"{main_h2m_dir}\\h2m-experiments\\app-data-placement\\XSBench\\results_numamma_alarm_100_sr_1000"
    # ===============================

    if not os.path.exists(args.source):
        print(f"Source folder path \"{args.source}\" does not exist")
        sys.exit(1)

    # save results in source folder
    if args.destination is None:
        args.destination = os.path.join(args.source, "recommendations")

    if not os.path.exists(args.destination):
        os.makedirs(args.destination)
    
    # finally create configuration based on args
    cfg = CConfig()
    cfg.set_config(args)

    # parsing data
    logger.info(f"Parse datasets ...")
    parse_data = CDataParser.parse_data(cfg)

    # init data container
    dc = CDataContainer(parse_data)

    # ===================================
    # === Plotting
    # ===================================
    sorted_allocs = sorted(dc.allocations, key=lambda x: x.date_alloc_ms, reverse=False)
    
    # DEBUG: FILTER
    # sorted_allocs = [x for x in sorted_allocs if x.size_mb > 2]
    # DEBUG: FILTER

    min_date_al = min([x.date_alloc_ms for x in sorted_allocs])
    max_date_al = max([x.date_dealloc_ms for x in sorted_allocs])
    min_date_ph = min([x.ts_ms for x in dc.phase_transitions])
    max_date_ph = max([x.ts_ms for x in dc.phase_transitions])
    
    min_date = min([min_date_al, min_date_ph])
    max_date = max([max_date_al, max_date_ph])

    lines = []
    colors = []
    num_allocs = len(sorted_allocs)
    num_phases = len(dc.phase_transitions)

    for idx in range(num_allocs):
        al = sorted_allocs[idx]
        lines.append(((al.date_alloc_ms, idx), (al.date_dealloc_ms, idx)))
        if idx % 2 == 0:
            colors.append("g")
        else:
            colors.append("b")
    
    for idx in range(num_phases):
        phtr = dc.phase_transitions[idx]
        lines.append(((phtr.ts_ms, 0), (phtr.ts_ms, num_allocs)))
        colors.append("r")
    
    ln_coll = matplotlib.collections.LineCollection(lines, colors=colors)
    fig = plt.figure(figsize=(16, 9),frameon=False)
    ax = plt.gca()
    ax.add_collection(ln_coll)
    ax.set_xlim(min_date-100, max_date+100)
    ax.set_ylim(-1, num_allocs+1)
    plt.ticklabel_format(style='plain')
    plt.draw()
    
    ax.set_xlabel("Time range")
    ax.set_ylabel("Allocations")
    ax.set_title("Overview of application allocation ranges")
    target_file_path = os.path.join(args.destination, "allocation_ranges.pdf")
    fig.savefig(target_file_path, dpi=None, facecolor='w', edgecolor='w', format="pdf", transparent=False, bbox_inches='tight', pad_inches=0, metadata=None)
    plt.close(fig)
    # plt.show()

    logger.info(f"Completed")